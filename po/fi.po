# Finnish translation for libgksu2
# Copyright (C) 2006 Rosetta Contributors and Canonical Ltd 2006
# This file is distributed under the same licence as the libgksu2 package.
#
msgid ""
msgstr ""
"Project-Id-Version: libgksu2\n"
"Report-Msgid-Bugs-To: kov@debian.org\n"
"POT-Creation-Date: 2017-05-04 16:02+0300\n"
"PO-Revision-Date: 2009-10-31 16:56+0000\n"
"Last-Translator: Jiri Grönroos <Unknown>\n"
"Language-Team: Finnish <fi@li.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/gksu-properties.desktop.in.h:1
msgid "Privilege granting"
msgstr "Oikeuksien myöntäminen"

#: ../data/gksu-properties.desktop.in.h:2
msgid "Configure behavior of the privilege-granting tool"
msgstr "Määrittele oikeuksien myöntämis -työkalun käyttäytyminen"

#: ../data/org.gksu.gschema.xml.in.h:1
msgid "Disable keyboard and mouse grab"
msgstr "Estä näppäimistön ja hiiren kaappaus"

#: ../data/org.gksu.gschema.xml.in.h:2
msgid ""
"Whether the keyboard and mouse grabbing should be turned off. This will make "
"it possible for other X applications to listen to keyboard input events, "
"thus making it not possible to shield from malicious applications which may "
"be running."
msgstr ""
"Kytketäänkö näppäimistön ja hiiren kaappaus pois päältä. Tämä mahdollistaa "
"muille X-ohjelmille näppäimistön tapahtumien lukemisen ja estää "
"suojautumisen haittaohjelmilta, jotka mahdollisesti ovat suorituksessa ja "
"yrittävät kerätä salasanoja."

#: ../data/org.gksu.gschema.xml.in.h:3
msgid "Force keyboard and mouse grab"
msgstr "Pakota näppäimistön ja hiiren kaappaus"

#: ../data/org.gksu.gschema.xml.in.h:4
msgid ""
"Grab keyboard and mouse even if -g has been passed as argument on the "
"command line."
msgstr ""
"Kaappaa näppäimistö ja hiiri myös silloin kun valitsin -g on annettu gksu:n "
"komentorivillä."

#: ../data/org.gksu.gschema.xml.in.h:5
msgid "Sudo mode"
msgstr "Sudo-tila"

#: ../data/org.gksu.gschema.xml.in.h:6
msgid ""
"Whether sudo should be the default backend method. This method is otherwise "
"accessed though the -S switch or by running 'gksudo' instead of 'gksu'."
msgstr ""
"Tuleeko sudon olla käytettävä oletusmenettely. Tämä menetelmä on muutoin "
"käytettävissä joko valitsimen -S avulla tai suorittamalla komento 'gksudo' "
"komennon 'gksu' sijasta."

#: ../data/org.gksu.gschema.xml.in.h:7
msgid "Prompt for grabbing"
msgstr "Vahvista kaappaus käyttäjältä"

#: ../data/org.gksu.gschema.xml.in.h:8
msgid ""
"This option will make gksu prompt the user if he wants to have the screen "
"grabbed before entering the password. Notice that this only has an effect if "
"force-grab is disabled."
msgstr ""
"Tämä vaihtoehto pakottaa gksu:n kysymään käyttäjältä kaapataanko istunto "
"ennen salasanan kysymistä. Huomaa, että tällä vaihtoehdolla ei ole "
"vaikutusta jos force-grab on käytössä."

#: ../data/org.gksu.gschema.xml.in.h:9
msgid "Display information message when no password is needed"
msgstr "Näytä tiedote, kun salasanaa ei tarvita"

#: ../data/org.gksu.gschema.xml.in.h:10
#, fuzzy
msgid ""
"This option determines whether a message dialog will be displayed informing "
"the user that the application is being run without the need of a password "
"being asked for some reason."
msgstr ""
"Tämä vaihtoehto määrittää, näytetäänkö käyttäjälle tiedote kun sovelluksen "
"suorittamiseksi ei jostain syystä tarvitse pyytää salasanaa."

#: ../data/org.gksu.gschema.xml.in.h:11
#, fuzzy
msgid "Save the password to the keyring"
msgstr "Tallenna salasana gnome-avainnippuun"

#: ../data/org.gksu.gschema.xml.in.h:12
#, fuzzy
msgid ""
"gksu can save the password you type to the keyring so you'll not be asked "
"everytime."
msgstr ""
"GKsu voi tallentaa syöttämäsi salasanan Gnomen avainnippuun, jolloin sitä ei "
"kysytä aina uudelleen."

#: ../data/org.gksu.gschema.xml.in.h:13
msgid "Keyring to which passwords will be saved"
msgstr "Avainnippu, johon salasanat tallennetaan"

#: ../data/org.gksu.gschema.xml.in.h:14
msgid ""
"The name of the keyring gksu should use. Usual values are \"session\", which "
"saves the password for the session, and \"default\", which saves the "
"password with no timeout."
msgstr ""
"Sen avainnipun nimi, jota GKsu:n pitäisi käyttää. Yleisiä arvoja ovat "
"\"session\", joka tallentaa salasanan istunnon ajaksi sekä \"default\", joka "
"tallentaa salasanan ilman aikarajaa."

#: ../gksu-properties/gksu-properties.c:262
msgid "Failed to load gtkui file; please check your installation."
msgstr ""
"Gtkui-tiedoston lataaminen epäonnistui. Ohjelman asennuksessa saattaa olla "
"jotain vialla."

#: ../gksu-properties/gksu-properties.ui.h:1
msgid "enable"
msgstr "ota käyttöön"

#: ../gksu-properties/gksu-properties.ui.h:2
msgid "disable"
msgstr "poista käytöstä"

#: ../gksu-properties/gksu-properties.ui.h:3
msgid "force enable"
msgstr "pakota käyttöön"

#: ../gksu-properties/gksu-properties.ui.h:4
msgid "prompt"
msgstr "kehote"

#: ../gksu-properties/gksu-properties.ui.h:5
msgid "su"
msgstr "su"

#: ../gksu-properties/gksu-properties.ui.h:6
msgid "sudo"
msgstr "sudo"

#: ../gksu-properties/gksu-properties.ui.h:7
msgid "_Authentication mode:"
msgstr "_Todentamistyyppi:"

#: ../gksu-properties/gksu-properties.ui.h:8
msgid "_Grab mode:"
msgstr "_Kaappaustyyppi:"

#: ../gksu-properties/gksu-properties.ui.h:9
msgid "<b>Screen Grabbing</b>"
msgstr "<b>Näytön haltuunotto</b>"

#: ../gksu-properties/gksu-properties.ui.h:10
msgid "<b>Behavior</b>"
msgstr "<b>Toiminta</b>"

#: ../gksu-properties/gksu-properties.ui.h:11
msgid "_Close"
msgstr "_Sulje"

#: ../libgksu/gksu-run-helper.c:132
#, c-format
msgid "Failed to obtain xauth key: xauth binary not found at usual locations"
msgstr ""
"xauth-avaimen saaminen epäonnistui: xauth-binääriä ei löydy "
"oletussijainneista"

#: ../libgksu/libgksu.c:152
#, c-format
msgid "Not using locking for read only lock file %s"
msgstr "Lukkoa ei käytetä kirjoitussuojatulle tiedostolle %s"

#: ../libgksu/libgksu.c:173
#, c-format
msgid "Not using locking for nfs mounted lock file %s"
msgstr "Ei lukitusta nfs-liitetylle lukitustiedostolle %s"

#: ../libgksu/libgksu.c:510
#, fuzzy
msgid ""
"<b><big>Could not grab your keyboard or mouse.</big></b>\n"
"\n"
"A malicious client may be eavesdropping on your session or you may have just "
"clicked a menu or some application just decided to get focus.\n"
"\n"
"Try again."
msgstr ""
"<b><big>Ei voitu ottaa haltuun näppäimistöä.</big></b>\n"
"\n"
"Pahantahtoinen sovellus voi olla seuraamassa istuntoa, jotain valikkoa oli "
"juuri napsautettu tai jokin ohjelma vain otti kohdistuksen itsellensä.\n"
"\n"
"Kaiken varalta yritä uudelleen."

#: ../libgksu/libgksu.c:872
#, c-format
msgid ""
"<b><big>Enter your password to perform administrative tasks</big></b>\n"
"\n"
"The application '%s' lets you modify essential parts of your system."
msgstr ""
"<b><big>Anna salasanasi järjestelmänhallintaa varten</big></b>\n"
"\n"
"Sovellus \"%s\" mahdollistaa järjestelmän keskeisten osien muokkaamisen."

#: ../libgksu/libgksu.c:879
#, c-format
msgid ""
"<b><big>Enter your password to run the application '%s' as user %s</big></b>"
msgstr ""
"<b><big>Anna salasanasi suorittaaksesi sovelluksen \"%s\" käyttäjänä %s</"
"big></b>"

#: ../libgksu/libgksu.c:888
#, c-format
msgid ""
"<b><big>Enter the administrative password</big></b>\n"
"\n"
"The application '%s' lets you modify essential parts of your system."
msgstr ""
"<b><big>Anna ylläpitosalasana</big></b>\n"
"\n"
"Sovellus \"%s\" mahdollistaa järjestelmän keskeisten osien muokkaamisen."

#: ../libgksu/libgksu.c:895
#, c-format
msgid "<b><big>Enter the password of %s to run the application '%s'</big></b>"
msgstr ""
"<b><big>Anna käyttäjän %s salasana suorittaaksesi sovelluksen \"%s\"</big></"
"b>"

#: ../libgksu/libgksu.c:937
#, c-format
msgid "Password prompt canceled."
msgstr "Salasanakysely peruttu."

#: ../libgksu/libgksu.c:998
#, c-format
msgid ""
"<b><big>Granted permissions without asking for password</big></b>\n"
"\n"
"The '%s' program was started with the privileges of the %s user without the "
"need to ask for a password, due to your system's authentication mechanism "
"setup.\n"
"\n"
"It is possible that you are being allowed to run specific programs as user "
"%s without the need for a password, or that the password is cached.\n"
"\n"
"This is not a problem report; it's simply a notification to make sure you "
"are aware of this."
msgstr ""
"<b><big>Oikeudet myönnetty kysymättä salasanaa</big></b>\n"
"\n"
"Ohjelma \"%s\" käynnistettiin käyttäjän %s oikeuksilla ilman salasanaa "
"johtuen järjestelmän varmennusmekanismin asetuksista.\n"
"\n"
"On mahdollisesti sallittua ajaa tiettyjä ohjelmia käyttäjänä %s ilman "
"salasanaa, tai niin että salasana on välimuistissa.\n"
"\n"
"Tämän viestin tarkoitus on vain huomauttaa asiasta, jotta käyttäjä tietää "
"mistä on kyse."

#: ../libgksu/libgksu.c:1019
msgid "Do _not display this message again"
msgstr "Älä _näytä tätä viestiä uudelleen"

#: ../libgksu/libgksu.c:1042
msgid ""
"<b>Would you like your screen to be \"grabbed\"\n"
"while you enter the password?</b>\n"
"\n"
"This means all applications will be paused to avoid\n"
"the eavesdropping of your password by a a malicious\n"
"application while you type it."
msgstr ""
"<b>Haluatko näytön olevan \"kahlittu\"\n"
"kun salasanaa syötetään?</b>\n"
"\n"
"Tämä tarkoittaa, että kaikki sovellukset ovat\n"
"pysäytettyjä mahdollisen pahantahtoisen\n"
"ohjelman salakuuntelun estämiseksi."

#: ../libgksu/libgksu.c:1825
#, c-format
msgid "gksu_run needs a command to be run, none was provided."
msgstr "gksu_run vaatii suoritettavan komennon, mutta komentoa ei annettu."

#: ../libgksu/libgksu.c:1833
#, c-format
msgid "The gksu-run-helper command was not found or is not executable."
msgstr "Komentoa gksu-run-helper ei löytynyt tai se ei ole suoritettavissa."

#: ../libgksu/libgksu.c:1841 ../libgksu/libgksu.c:2379
#, c-format
msgid "Unable to copy the user's Xauthorization file."
msgstr "Käyttäjän .Xauthority-tiedostoa ei voi kopioida."

#: ../libgksu/libgksu.c:1890 ../libgksu/libgksu.c:2563
#, c-format
msgid "Failed to fork new process: %s"
msgstr "Uutta prosessia ei voitu luoda: %s"

#: ../libgksu/libgksu.c:2047
#, c-format
msgid "Wrong password got from keyring."
msgstr "Avainrenkaasta vastaanotettiin väärä salasana."

#: ../libgksu/libgksu.c:2051 ../libgksu/libgksu.c:2055
#: ../libgksu/libgksu.c:2746
#, c-format
msgid "Wrong password."
msgstr "Väärä salasana."

#: ../libgksu/libgksu.c:2111 ../libgksu/libgksu.c:2125
#, c-format
msgid ""
"Failed to communicate with gksu-run-helper.\n"
"\n"
"Received:\n"
" %s\n"
"While expecting:\n"
" %s"
msgstr ""
"Yhteydessä ohjelmaan gksu-run-helper oli ongelmia:\n"
"\n"
"Vastaanotettu:\n"
" %s\n"
"Odotettiin:\n"
" %s"

#: ../libgksu/libgksu.c:2117
#, c-format
msgid ""
"Failed to communicate with gksu-run-helper.\n"
"\n"
"Received bad string while expecting:\n"
" %s"
msgstr ""
"Yhteydessä ohjelmaan gksu-run-helper oli ongelmia.\n"
"\n"
"Vastaanotettiin virheellinen viesti, kun odotettiin seuraavaa:\n"
" %s"

#: ../libgksu/libgksu.c:2227
#, c-format
msgid "su terminated with %d status"
msgstr "su lopetti virhekoodiin %d"

#: ../libgksu/libgksu.c:2350
#, c-format
msgid "gksu_sudo_run needs a command to be run, none was provided."
msgstr "gksu_sudo_run vaatii suoritettavan komennon, mutta komentoa ei annettu"

#: ../libgksu/libgksu.c:2514 ../libgksu/libgksu.c:2523
#, c-format
msgid "Error creating pipe: %s"
msgstr "Virhe luotaessa putkea: %s"

#: ../libgksu/libgksu.c:2554
#, c-format
msgid "Failed to exec new process: %s"
msgstr "Uutta prosessia ei voitu suorittaa: %s"

#: ../libgksu/libgksu.c:2587 ../libgksu/libgksu.c:2598
#, c-format
msgid "Error opening pipe: %s"
msgstr "Virhe avattaessa putkea: %s"

#: ../libgksu/libgksu.c:2666
msgid "Password: "
msgstr "Salasana: "

#: ../libgksu/libgksu.c:2759 ../libgksu/libgksu.c:2823
#, c-format
msgid ""
"The underlying authorization mechanism (sudo) does not allow you to run this "
"program. Contact the system administrator."
msgstr ""
"Oikeuksien hallinta (sudo) ei salli sinun suorittaa tätä ohjelmaa. Ota "
"yhteys järjestelmänvalvojaan."

#: ../libgksu/libgksu.c:2843
#, c-format
msgid "sudo terminated with %d status"
msgstr "sudo päättyi virhekoodiin %d"

#: ../libgksu/libgksu-dialog.c:162
msgid "<b>You have capslock on</b>"
msgstr "<b>Isot kirjaimet (caps lock) ovat käytössä</b>"

#: ../libgksu/libgksu-dialog.c:267
msgid "Remember password"
msgstr "Muista salasana"

#: ../libgksu/libgksu-dialog.c:297
msgid "Save for this session"
msgstr "Tallenna tämän istunnon ajaksi"

#: ../libgksu/libgksu-dialog.c:305
msgid "Save in the keyring"
msgstr "Tallenna avainnippuun"

#: ../libgksu/libgksu-dialog.c:387
msgid "_Cancel"
msgstr "_Peru"

#: ../libgksu/libgksu-dialog.c:394
msgid "_OK"
msgstr "_OK"

#. label
#: ../libgksu/libgksu-dialog.c:434
msgid "<span weight=\"bold\" size=\"larger\">Type the root password.</span>\n"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Anna pääkäyttäjän salasana.</span>\n"

#. entry label
#: ../libgksu/libgksu-dialog.c:474
msgid "Password:"
msgstr "Salasana:"

#: ../libgksu/libgksu-dialog.c:549
msgid "Granting Rights"
msgstr "Myönnetään oikeuksia"

#~ msgid ""
#~ "<b><big>Could not grab your mouse.</big></b>\n"
#~ "\n"
#~ "A malicious client may be eavesdropping on your session or you may have "
#~ "just clicked a menu or some application just decided to get focus.\n"
#~ "\n"
#~ "Try again."
#~ msgstr ""
#~ "<b><big>Ei voitu ottaa haltuun hiirtä.</big></b>\n"
#~ "\n"
#~ "Pahantahtoinen sovellus voi olla seuraamassa istuntoa, jotain valikkoa "
#~ "oli juuri napsautettu tai jokin ohjelma vain otti kohdistuksen "
#~ "itsellensä.\n"
#~ "\n"
#~ "Kaiken varalta yritä uudelleen."
