# Albanian translation for libgksu2
# Copyright (C) 2007 Rosetta Contributors and Canonical Ltd 2007
# This file is distributed under the same licence as the libgksu2 package.
#
msgid ""
msgstr ""
"Project-Id-Version: libgksu2\n"
"Report-Msgid-Bugs-To: kov@debian.org\n"
"POT-Creation-Date: 2017-05-04 16:03+0300\n"
"PO-Revision-Date: 2009-07-16 18:25+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/gksu-properties.desktop.in.h:1
msgid "Privilege granting"
msgstr "Duke dhënë privilegje"

#: ../data/gksu-properties.desktop.in.h:2
msgid "Configure behavior of the privilege-granting tool"
msgstr "Konfiguro sjelljen e mjetit të dhënies së privilegjeve"

#: ../data/org.gksu.gschema.xml.in.h:1
msgid "Disable keyboard and mouse grab"
msgstr "Çaktivizo kapjen nga miu dhe tastiera"

#: ../data/org.gksu.gschema.xml.in.h:2
msgid ""
"Whether the keyboard and mouse grabbing should be turned off. This will make "
"it possible for other X applications to listen to keyboard input events, "
"thus making it not possible to shield from malicious applications which may "
"be running."
msgstr ""
"Nëse kapja e tastierës apo miut duhet çaktivizuar. Kjo do të mundësojë që "
"programet e tjera X të dëgjojnë tastet e klikuara nga tastiera juaj, duke e "
"bërë rë pamundur që të mbroheni nga programet keqdashëse që mund të jenë "
"aktivizuar."

#: ../data/org.gksu.gschema.xml.in.h:3
msgid "Force keyboard and mouse grab"
msgstr "Detyro kapjen e miut dhe tastierës"

#: ../data/org.gksu.gschema.xml.in.h:4
msgid ""
"Grab keyboard and mouse even if -g has been passed as argument on the "
"command line."
msgstr ""
"Kap tastierën dhe miun edhe pse -g ka kaluar si një argument në rreshtin e "
"komandave."

#: ../data/org.gksu.gschema.xml.in.h:5
msgid "Sudo mode"
msgstr "Mënyra sudo"

#: ../data/org.gksu.gschema.xml.in.h:6
msgid ""
"Whether sudo should be the default backend method. This method is otherwise "
"accessed though the -S switch or by running 'gksudo' instead of 'gksu'."
msgstr ""
"Nëse sudo duhet të jetë metoda e parazgjedhur mbështetëse. kjo metodë "
"aktivizohet ndryshe me anë të çelësit -S ose duke nisur 'gksudo' në vend të "
"'gksu'."

#: ../data/org.gksu.gschema.xml.in.h:7
msgid "Prompt for grabbing"
msgstr "Pyet për kapjen"

#: ../data/org.gksu.gschema.xml.in.h:8
msgid ""
"This option will make gksu prompt the user if he wants to have the screen "
"grabbed before entering the password. Notice that this only has an effect if "
"force-grab is disabled."
msgstr ""
"Ky opsion do të bëjë që gksu të pyesë përdoruesin nëse dëshiron t'i "
"regjistrohet ekrani para se të vendosë fjalëkalimin. Vini re që kjo është "
"efektive vetëm nëse force-grab çaktivizohet."

#: ../data/org.gksu.gschema.xml.in.h:9
msgid "Display information message when no password is needed"
msgstr "Shfaq një mesazh informues kur nevojitet fjalëkalimi"

#: ../data/org.gksu.gschema.xml.in.h:10
#, fuzzy
msgid ""
"This option determines whether a message dialog will be displayed informing "
"the user that the application is being run without the need of a password "
"being asked for some reason."
msgstr ""
"Ky opsion përcakton nëse një mesazh dialogu duhet të shfaqet për ta "
"informuar përdoruesin se programi po niset pa patur nevojë që të kërkohet "
"fjalëkalimi për ndonjë arsye."

#: ../data/org.gksu.gschema.xml.in.h:11
#, fuzzy
msgid "Save the password to the keyring"
msgstr "Regjistroje fjalëkalimin tek gnome-keyring"

#: ../data/org.gksu.gschema.xml.in.h:12
#, fuzzy
msgid ""
"gksu can save the password you type to the keyring so you'll not be asked "
"everytime."
msgstr ""
"gksu mund ta ndryshojë fjalëkalimin që ju shtypni në gnome-keyring kështu që "
"ju nuk do të pyeteni përherë"

#: ../data/org.gksu.gschema.xml.in.h:13
msgid "Keyring to which passwords will be saved"
msgstr "Rreshti në të cilin do të regjistrohet fjalëkalimi"

#: ../data/org.gksu.gschema.xml.in.h:14
msgid ""
"The name of the keyring gksu should use. Usual values are \"session\", which "
"saves the password for the session, and \"default\", which saves the "
"password with no timeout."
msgstr ""
"Emri i keyring që gksu duhet të përdorë. Vlerat e zakonshme janë \"seksion"
"\", i cili regjistron fjalëkalimet për këtë seksion dhe \"i parazgjedhur\", "
"i cili i regjistron fjalëkalimet pa afat kohor."

#: ../gksu-properties/gksu-properties.c:262
msgid "Failed to load gtkui file; please check your installation."
msgstr ""
"Dështuam në ngarkimin e skedarit gtkui; ju lutemi të kontrolloni instalimin "
"tuaj."

#: ../gksu-properties/gksu-properties.ui.h:1
msgid "enable"
msgstr "aktivizo"

#: ../gksu-properties/gksu-properties.ui.h:2
msgid "disable"
msgstr "çaktivizo"

#: ../gksu-properties/gksu-properties.ui.h:3
msgid "force enable"
msgstr "detyro aktivizimin"

#: ../gksu-properties/gksu-properties.ui.h:4
msgid "prompt"
msgstr "prompt"

#: ../gksu-properties/gksu-properties.ui.h:5
msgid "su"
msgstr "su"

#: ../gksu-properties/gksu-properties.ui.h:6
msgid "sudo"
msgstr "sudo"

#: ../gksu-properties/gksu-properties.ui.h:7
msgid "_Authentication mode:"
msgstr "_Mënyra e Autemtikimit:"

#: ../gksu-properties/gksu-properties.ui.h:8
msgid "_Grab mode:"
msgstr "_Mënyra e kapjes:"

#: ../gksu-properties/gksu-properties.ui.h:9
msgid "<b>Screen Grabbing</b>"
msgstr "<b>Kapje e Ekranit</b>"

#: ../gksu-properties/gksu-properties.ui.h:10
msgid "<b>Behavior</b>"
msgstr "<b>Sjellja</b>"

#: ../gksu-properties/gksu-properties.ui.h:11
msgid "_Close"
msgstr "_Mbyll"

#: ../libgksu/gksu-run-helper.c:132
#, c-format
msgid "Failed to obtain xauth key: xauth binary not found at usual locations"
msgstr ""
"Dështuam në marrjen e çelësit xauth: xauth binar nuk u gjet në vendndodhjet "
"e zakonshme"

#: ../libgksu/libgksu.c:152
#, c-format
msgid "Not using locking for read only lock file %s"
msgstr ""
"Nuk po përdorim bllokimin për të bllokuar skedarin vetëm të lexueshëm %s"

#: ../libgksu/libgksu.c:173
#, c-format
msgid "Not using locking for nfs mounted lock file %s"
msgstr "Nuk po përdorim bllokimin për skedarin e bllokuar të montuar nfs %s"

#: ../libgksu/libgksu.c:510
#, fuzzy
msgid ""
"<b><big>Could not grab your keyboard or mouse.</big></b>\n"
"\n"
"A malicious client may be eavesdropping on your session or you may have just "
"clicked a menu or some application just decided to get focus.\n"
"\n"
"Try again."
msgstr ""
"<b><big>Nuk mund të kapim tastierën.</big></b>\n"
"\n"
"Një klient keqdashës mund të përgjojë seksionin tuaj ose mund të ketë "
"klikuar në një menu dhe një program thjesht vendosi të fokusohet.\n"
"\n"
"Provoje përsëri."

#: ../libgksu/libgksu.c:872
#, c-format
msgid ""
"<b><big>Enter your password to perform administrative tasks</big></b>\n"
"\n"
"The application '%s' lets you modify essential parts of your system."
msgstr ""
"<b><big>Vendosni fjalëkalimin tuaj për të kryer detyra administrative</big></"
"b>\n"
"\n"
"Programi '%s' ju lë të modifikoni pjesë thelbësore të sistemit tuaj."

#: ../libgksu/libgksu.c:879
#, c-format
msgid ""
"<b><big>Enter your password to run the application '%s' as user %s</big></b>"
msgstr ""
"<b><big>Vendosni fjalëkalimin tuaj për të nisur programin '%s' si përdoruesi "
"%s</big></b>"

#: ../libgksu/libgksu.c:888
#, c-format
msgid ""
"<b><big>Enter the administrative password</big></b>\n"
"\n"
"The application '%s' lets you modify essential parts of your system."
msgstr ""
"<b><big>Vendosni fjalëkalimin e administruesit</big></b>\n"
"\n"
"Programi '%s' ju lë të modifikoni pjesë thelbësore të sistemit tuaj."

#: ../libgksu/libgksu.c:895
#, c-format
msgid "<b><big>Enter the password of %s to run the application '%s'</big></b>"
msgstr ""
"<b><big>Vendosni fjalëkalimin e %s për të nisur programin '%s'</big></b>"

#: ../libgksu/libgksu.c:937
#, c-format
msgid "Password prompt canceled."
msgstr "Kërkimi për fjalëkalim u fshi."

#: ../libgksu/libgksu.c:998
#, c-format
msgid ""
"<b><big>Granted permissions without asking for password</big></b>\n"
"\n"
"The '%s' program was started with the privileges of the %s user without the "
"need to ask for a password, due to your system's authentication mechanism "
"setup.\n"
"\n"
"It is possible that you are being allowed to run specific programs as user "
"%s without the need for a password, or that the password is cached.\n"
"\n"
"This is not a problem report; it's simply a notification to make sure you "
"are aware of this."
msgstr ""
"<b><big>Leja është dhënë pa kërkuar për fjalëkalim</big></b>\n"
"\n"
"Programi '%s' u nis me privilegjet e përdoruesit %s pa patur nevojë për të "
"kërkuar një fjalëkalim, për shkak të mekanizmit të instalimit të "
"autentifikimit të sistemit tuaj.\n"
"\n"
"Ka mundësi që ju të lejoheni të nisni programe specifike si përdoruesi %s pa "
"patur nevojën për një fjalëkalim, ose me fjalëkalimin e ruajtur që më parë.\n"
"\n"
"Ky nuk është një raportim problemi; është thjesht një lajmërim për tu "
"siguruar që ju jeni në dijeni të kësaj."

#: ../libgksu/libgksu.c:1019
msgid "Do _not display this message again"
msgstr "mos _bej qe te kespozohet ky mesazh perseri"

#: ../libgksu/libgksu.c:1042
msgid ""
"<b>Would you like your screen to be \"grabbed\"\n"
"while you enter the password?</b>\n"
"\n"
"This means all applications will be paused to avoid\n"
"the eavesdropping of your password by a a malicious\n"
"application while you type it."
msgstr ""
"<b>Dëshironi t'iu \"kapet\" ekrani\n"
"kur ju vendosni fjalëkalimin?</b>\n"
"\n"
"Kjo do të thotë që të gjitha programet do të vendosen në pauzë për të "
"evituar\n"
"mbikqyrjen e fjalëkalimit tuaj nga një program\n"
"keqdashës ndërkohë që ju e shtypni atë."

#: ../libgksu/libgksu.c:1825
#, c-format
msgid "gksu_run needs a command to be run, none was provided."
msgstr ""
"gksu_run ka nevojë për një komandë për tu ekzekutuar, por nuk është dhënë "
"asnjë komandë."

#: ../libgksu/libgksu.c:1833
#, c-format
msgid "The gksu-run-helper command was not found or is not executable."
msgstr ""
"Komanda gksu-run-helper nuk është gjetur ose nuk është e ekzekutueshme."

#: ../libgksu/libgksu.c:1841 ../libgksu/libgksu.c:2379
#, c-format
msgid "Unable to copy the user's Xauthorization file."
msgstr ""
"Është e pamundur të kopjohet skedari Xauthorization nga përdoruesi i "
"kompjuterit."

#: ../libgksu/libgksu.c:1890 ../libgksu/libgksu.c:2563
#, c-format
msgid "Failed to fork new process: %s"
msgstr "Dështoi përpunimi i proçesit të ri: %s"

#: ../libgksu/libgksu.c:2047
#, c-format
msgid "Wrong password got from keyring."
msgstr "U morr një fjalëkalim i gabuar nga rreshti i çelësave."

#: ../libgksu/libgksu.c:2051 ../libgksu/libgksu.c:2055
#: ../libgksu/libgksu.c:2746
#, c-format
msgid "Wrong password."
msgstr "Pasvord i gabuar"

#: ../libgksu/libgksu.c:2111 ../libgksu/libgksu.c:2125
#, c-format
msgid ""
"Failed to communicate with gksu-run-helper.\n"
"\n"
"Received:\n"
" %s\n"
"While expecting:\n"
" %s"
msgstr ""
"Dështoi komunikimi me gksu-run-helper.\n"
"\n"
"Përgjigja që u morr është:\n"
" %s\n"
"Duke pritur:\n"
" %s"

#: ../libgksu/libgksu.c:2117
#, c-format
msgid ""
"Failed to communicate with gksu-run-helper.\n"
"\n"
"Received bad string while expecting:\n"
" %s"
msgstr ""
"Dështuam në komunikimin me gksu-run-helper.\n"
"\n"
"Morëm një rresht të dëmtuar ndërkohë që pritej:\n"
" %s"

#: ../libgksu/libgksu.c:2227
#, c-format
msgid "su terminated with %d status"
msgstr "su mbaroi me rezultatin %d"

#: ../libgksu/libgksu.c:2350
#, c-format
msgid "gksu_sudo_run needs a command to be run, none was provided."
msgstr ""
"gksu_sudo_run ka nevojë për një komandë, por nuk është dhënë asnjë komandë."

#: ../libgksu/libgksu.c:2514 ../libgksu/libgksu.c:2523
#, c-format
msgid "Error creating pipe: %s"
msgstr "Gabim në krijimin e linjës: %s"

#: ../libgksu/libgksu.c:2554
#, c-format
msgid "Failed to exec new process: %s"
msgstr "Dështoi ekzekutimi i proçesit të ri: %s"

#: ../libgksu/libgksu.c:2587 ../libgksu/libgksu.c:2598
#, c-format
msgid "Error opening pipe: %s"
msgstr "Gabim në hapjen e linjës: %s"

#: ../libgksu/libgksu.c:2666
msgid "Password: "
msgstr "Fjalëkalimi: "

#: ../libgksu/libgksu.c:2759 ../libgksu/libgksu.c:2823
#, c-format
msgid ""
"The underlying authorization mechanism (sudo) does not allow you to run this "
"program. Contact the system administrator."
msgstr ""
"Mekanizmi i identifikimit këtu poshtë (sudo) nuk lejon ekzekutimin e këtij "
"programi. Kontaktoni administratorin e sistemit."

#: ../libgksu/libgksu.c:2843
#, c-format
msgid "sudo terminated with %d status"
msgstr "sudo mbaroi me rezultatin %d"

#: ../libgksu/libgksu-dialog.c:162
msgid "<b>You have capslock on</b>"
msgstr "<b>Ju keni simbolet e mëdha aktive</b>"

#: ../libgksu/libgksu-dialog.c:267
msgid "Remember password"
msgstr "Kujto fjalëkalimin"

#: ../libgksu/libgksu-dialog.c:297
msgid "Save for this session"
msgstr "Ruaje për këtë seksion"

#: ../libgksu/libgksu-dialog.c:305
msgid "Save in the keyring"
msgstr "Regjistro kodin"

#: ../libgksu/libgksu-dialog.c:387
msgid "_Cancel"
msgstr "A_nullo"

#: ../libgksu/libgksu-dialog.c:394
msgid "_OK"
msgstr "_OK"

#. label
#: ../libgksu/libgksu-dialog.c:434
msgid "<span weight=\"bold\" size=\"larger\">Type the root password.</span>\n"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Shtyp fjalëkalimin e rrënjës.</span>\n"

#. entry label
#: ../libgksu/libgksu-dialog.c:474
msgid "Password:"
msgstr "Fjalëkalimi:"

#: ../libgksu/libgksu-dialog.c:549
msgid "Granting Rights"
msgstr "Duke Dhënë të Drejta"

#~ msgid ""
#~ "<b><big>Could not grab your mouse.</big></b>\n"
#~ "\n"
#~ "A malicious client may be eavesdropping on your session or you may have "
#~ "just clicked a menu or some application just decided to get focus.\n"
#~ "\n"
#~ "Try again."
#~ msgstr ""
#~ "<b><big>Nuk mundëm të kapim miun.</big></b>\n"
#~ "\n"
#~ "Një klient keqdashës mund të përgjojë seksionin tuaj ose mund të ketë "
#~ "klikuar në një menu dhe një program thjesht vendosi të fokusohet.\n"
#~ "\n"
#~ "Provoje përsëri."
