# Asturian translation for libgksu2
# Copyright (C) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same licence as the libgksu2 package.
#
msgid ""
msgstr ""
"Project-Id-Version: libgksu2\n"
"Report-Msgid-Bugs-To: kov@debian.org\n"
"POT-Creation-Date: 2017-05-04 16:02+0300\n"
"PO-Revision-Date: 2010-03-18 22:15+0000\n"
"Last-Translator: ivarela <ivarela@ubuntu.com>\n"
"Language-Team: Asturian <ast@li.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/gksu-properties.desktop.in.h:1
msgid "Privilege granting"
msgstr "Otorgar privilexos"

#: ../data/gksu-properties.desktop.in.h:2
msgid "Configure behavior of the privilege-granting tool"
msgstr "Configura el comportamientu de la ferramienta pa conceder privilexos"

#: ../data/org.gksu.gschema.xml.in.h:1
msgid "Disable keyboard and mouse grab"
msgstr "Desactivar grabar tecláu y mur"

#: ../data/org.gksu.gschema.xml.in.h:2
msgid ""
"Whether the keyboard and mouse grabbing should be turned off. This will make "
"it possible for other X applications to listen to keyboard input events, "
"thus making it not possible to shield from malicious applications which may "
"be running."
msgstr ""
"Si la captura del tecláu y el mur tienen de ser desactivaes. Esto fadrá "
"dable qu'otres aplicaciones X escuchen la entrada del tecláu faciendo "
"nondable protexese d'aplicaciones malicioses que pudieren tar executándose."

#: ../data/org.gksu.gschema.xml.in.h:3
msgid "Force keyboard and mouse grab"
msgstr "Forzar grabar tecláu y mur"

#: ../data/org.gksu.gschema.xml.in.h:4
msgid ""
"Grab keyboard and mouse even if -g has been passed as argument on the "
"command line."
msgstr ""
"Garrar tecláu y ratón incluso si se pasara -g como argumentu na llinia de "
"comandu."

#: ../data/org.gksu.gschema.xml.in.h:5
msgid "Sudo mode"
msgstr "Modu sudo"

#: ../data/org.gksu.gschema.xml.in.h:6
msgid ""
"Whether sudo should be the default backend method. This method is otherwise "
"accessed though the -S switch or by running 'gksudo' instead of 'gksu'."
msgstr ""
"Si sudo tien de ser el métodu por defeutu. D'otra miente, pue accedese a "
"esti métodu usando l'opción -S o executando «gksudo» n'arróu de «gksu»."

#: ../data/org.gksu.gschema.xml.in.h:7
msgid "Prompt for grabbing"
msgstr "Entrugar enantes de capturar"

#: ../data/org.gksu.gschema.xml.in.h:8
msgid ""
"This option will make gksu prompt the user if he wants to have the screen "
"grabbed before entering the password. Notice that this only has an effect if "
"force-grab is disabled."
msgstr ""
"Esta opción fadrá que gksu entrugue al usuariu si deseya que la pantalla "
"seya capturada enantes d'introducir la contraseña. Note qu'esto namái ye "
"efeutivo si la captura forzada ta deshabilitada."

#: ../data/org.gksu.gschema.xml.in.h:9
msgid "Display information message when no password is needed"
msgstr "Amosar mensaxe d'información cuando nun se necesite una contraseña"

#: ../data/org.gksu.gschema.xml.in.h:10
msgid ""
"This option determines whether a message dialog will be displayed informing "
"the user that the application is being run without the need of a password "
"being asked for some reason."
msgstr ""
"Esta opción determina si un diálogu de mensaxe s'amuesa informando al "
"usuariu que'l anwendung ta executándose ensin necesitar pidir una contraseña "
"por cualesquier razón."

#: ../data/org.gksu.gschema.xml.in.h:11
msgid "Save the password to the keyring"
msgstr "Guardar contraseña del anillu de claves"

#: ../data/org.gksu.gschema.xml.in.h:12
#, fuzzy
msgid ""
"gksu can save the password you type to the keyring so you'll not be asked "
"everytime."
msgstr ""
"gksu puede guardar la contraseña que teclees nel anillu de claves, asina nun "
"entrugará cada vez"

#: ../data/org.gksu.gschema.xml.in.h:13
msgid "Keyring to which passwords will be saved"
msgstr "Anillu de claves nel que se guardarán les contraseñes"

#: ../data/org.gksu.gschema.xml.in.h:14
msgid ""
"The name of the keyring gksu should use. Usual values are \"session\", which "
"saves the password for the session, and \"default\", which saves the "
"password with no timeout."
msgstr ""
"El nome del anillu de claves que gksu tien d'emplegar. Los valores normales "
"son \"session\", que guarda la contraseña pa la sesión, y \"default\", que "
"guarda la contraseña ensin que caduque."

#: ../gksu-properties/gksu-properties.c:262
msgid "Failed to load gtkui file; please check your installation."
msgstr "Nun pudo cargase'l ficheru gtkui; verifique la so instalación."

#: ../gksu-properties/gksu-properties.ui.h:1
msgid "enable"
msgstr "habilitar"

#: ../gksu-properties/gksu-properties.ui.h:2
msgid "disable"
msgstr "deshabilitar"

#: ../gksu-properties/gksu-properties.ui.h:3
msgid "force enable"
msgstr "forciar l'activación"

#: ../gksu-properties/gksu-properties.ui.h:4
msgid "prompt"
msgstr "prompt"

#: ../gksu-properties/gksu-properties.ui.h:5
msgid "su"
msgstr "su"

#: ../gksu-properties/gksu-properties.ui.h:6
msgid "sudo"
msgstr "sudo"

#: ../gksu-properties/gksu-properties.ui.h:7
msgid "_Authentication mode:"
msgstr "Modu d'autentificación"

#: ../gksu-properties/gksu-properties.ui.h:8
msgid "_Grab mode:"
msgstr "_Mou d'afitar:"

#: ../gksu-properties/gksu-properties.ui.h:9
msgid "<b>Screen Grabbing</b>"
msgstr "<b>Afitar pantalla</b>"

#: ../gksu-properties/gksu-properties.ui.h:10
msgid "<b>Behavior</b>"
msgstr "<b>Comportamientu</b>"

#: ../gksu-properties/gksu-properties.ui.h:11
msgid "_Close"
msgstr "_Zarrar"

#: ../libgksu/gksu-run-helper.c:132
#, c-format
msgid "Failed to obtain xauth key: xauth binary not found at usual locations"
msgstr ""
"Nun pudo garrase la llave xauth: binarios xauth nun atopaos nel sitiu usual"

#: ../libgksu/libgksu.c:152
#, c-format
msgid "Not using locking for read only lock file %s"
msgstr "Nun uses bloquear pa sólo bloquear la llectura del ficheru %s"

#: ../libgksu/libgksu.c:173
#, c-format
msgid "Not using locking for nfs mounted lock file %s"
msgstr "Nun usar bloqueos pal archivu bloqueau na nfs montada %s"

#: ../libgksu/libgksu.c:510
msgid ""
"<b><big>Could not grab your keyboard or mouse.</big></b>\n"
"\n"
"A malicious client may be eavesdropping on your session or you may have just "
"clicked a menu or some application just decided to get focus.\n"
"\n"
"Try again."
msgstr ""
"<b><big>Nun se pudo bloquiar el so tecláu oder ratón.</big></b>\n"
"\n"
"Un cliente maliciosu quiciabes tesa chisbando na so sesión o quiciabes acaba "
"de calcar nún menú o dalguna aplicación xusto cuando decidió capturar el "
"focu.\n"
"\n"
"Téntelo de nuevu."

#: ../libgksu/libgksu.c:872
#, c-format
msgid ""
"<b><big>Enter your password to perform administrative tasks</big></b>\n"
"\n"
"The application '%s' lets you modify essential parts of your system."
msgstr ""
"<b><big>Introduz la to clave pa facer xeres alministratives</big></b>\n"
"\n"
"L'aplicación '%s' va modificar partes esenciales del to sistema."

#: ../libgksu/libgksu.c:879
#, c-format
msgid ""
"<b><big>Enter your password to run the application '%s' as user %s</big></b>"
msgstr ""
"<b><big>Introduz la to clave pa executar l'aplicación '%s' como usuariu %s</"
"big></b>"

#: ../libgksu/libgksu.c:888
#, c-format
msgid ""
"<b><big>Enter the administrative password</big></b>\n"
"\n"
"The application '%s' lets you modify essential parts of your system."
msgstr ""
"<b><big>Introduz la clave alministrativa</big></b>\n"
"\n"
"L'aplicación '%s' va modificar partes esenciales del to sistema."

#: ../libgksu/libgksu.c:895
#, c-format
msgid "<b><big>Enter the password of %s to run the application '%s'</big></b>"
msgstr ""
"<b><big>Introduz la clave de %s pa executar l'aplicación '%s'</big></b>"

#: ../libgksu/libgksu.c:937
#, c-format
msgid "Password prompt canceled."
msgstr "Petición de contraseña encaboxada"

#: ../libgksu/libgksu.c:998
#, c-format
msgid ""
"<b><big>Granted permissions without asking for password</big></b>\n"
"\n"
"The '%s' program was started with the privileges of the %s user without the "
"need to ask for a password, due to your system's authentication mechanism "
"setup.\n"
"\n"
"It is possible that you are being allowed to run specific programs as user "
"%s without the need for a password, or that the password is cached.\n"
"\n"
"This is not a problem report; it's simply a notification to make sure you "
"are aware of this."
msgstr ""
"<b><big>Permisos concedíos ensin solicitar la contraseña</big></b>\n"
"\n"
"El programa «%s» anicióse colos privilexos del usuariu %s ensin que fore "
"necesario pedir una contraseña, per aciu de la configuración del mecanismu "
"d'autentificación del sistema.\n"
"\n"
"Ye dable que-y tea permitío executar dalgunos programes como l'usuariu %s "
"ensin necesidá dala de contraseña, o ensin que la contraseña tea "
"almacenada.\n"
"\n"
"Esto nun ye un informe d'error, sinón cenciellamente un avisu p'asegurase de "
"que ta al corriente."

#: ../libgksu/libgksu.c:1019
msgid "Do _not display this message again"
msgstr "_Nun amosar esti mensaxe de nuevo"

#: ../libgksu/libgksu.c:1042
msgid ""
"<b>Would you like your screen to be \"grabbed\"\n"
"while you enter the password?</b>\n"
"\n"
"This means all applications will be paused to avoid\n"
"the eavesdropping of your password by a a malicious\n"
"application while you type it."
msgstr ""
"<b>¿Quier que la so pantalla seya «bloquiada»\n"
"cuando introduzas la contraseña?</b>\n"
"\n"
"Esto significa que toles aplicaciones van pausase\n"
"pa evitar l'espionaxe de la contraseña por una\n"
"aplicación maliciosa mientres la teclees."

#: ../libgksu/libgksu.c:1825
#, c-format
msgid "gksu_run needs a command to be run, none was provided."
msgstr "gksu_run necesita un comandu pa executase, y nun se dio nenguna."

#: ../libgksu/libgksu.c:1833
#, c-format
msgid "The gksu-run-helper command was not found or is not executable."
msgstr "El comandu gksu-run-helper nun s'atopó o nun ye executable."

#: ../libgksu/libgksu.c:1841 ../libgksu/libgksu.c:2379
#, c-format
msgid "Unable to copy the user's Xauthorization file."
msgstr "Incapaz de copiar el ficheru Xauthorization del usuariu."

#: ../libgksu/libgksu.c:1890 ../libgksu/libgksu.c:2563
#, c-format
msgid "Failed to fork new process: %s"
msgstr "Falló la división del nuevu procesu: %s"

#: ../libgksu/libgksu.c:2047
#, c-format
msgid "Wrong password got from keyring."
msgstr "Clave mal tecleada."

#: ../libgksu/libgksu.c:2051 ../libgksu/libgksu.c:2055
#: ../libgksu/libgksu.c:2746
#, c-format
msgid "Wrong password."
msgstr "Contraseña incorreuta."

#: ../libgksu/libgksu.c:2111 ../libgksu/libgksu.c:2125
#, c-format
msgid ""
"Failed to communicate with gksu-run-helper.\n"
"\n"
"Received:\n"
" %s\n"
"While expecting:\n"
" %s"
msgstr ""
"Falló la comunicación col gksu-run-helper.\n"
"\n"
"Recibió:\n"
" %s\n"
"Mientres esperaba:\n"
" %s"

#: ../libgksu/libgksu.c:2117
#, c-format
msgid ""
"Failed to communicate with gksu-run-helper.\n"
"\n"
"Received bad string while expecting:\n"
" %s"
msgstr ""
"Falló al comunicar con gksu-run-helper.\n"
"\n"
"Recibióse una cadena errónea mentres s'esperaba:\n"
"%s"

#: ../libgksu/libgksu.c:2227
#, c-format
msgid "su terminated with %d status"
msgstr "su terminó col estáu %d"

#: ../libgksu/libgksu.c:2350
#, c-format
msgid "gksu_sudo_run needs a command to be run, none was provided."
msgstr ""
"gksu_sudo_run necesita un comandu pa executase, nun se proporcionó nengún."

#: ../libgksu/libgksu.c:2514 ../libgksu/libgksu.c:2523
#, c-format
msgid "Error creating pipe: %s"
msgstr "Error al crear el pipe: %s"

#: ../libgksu/libgksu.c:2554
#, c-format
msgid "Failed to exec new process: %s"
msgstr "Falló la execución del nuevu procesu: %s"

#: ../libgksu/libgksu.c:2587 ../libgksu/libgksu.c:2598
#, c-format
msgid "Error opening pipe: %s"
msgstr "Error al abrir el conductu: %s"

#: ../libgksu/libgksu.c:2666
msgid "Password: "
msgstr "Contraseña: "

#: ../libgksu/libgksu.c:2759 ../libgksu/libgksu.c:2823
#, c-format
msgid ""
"The underlying authorization mechanism (sudo) does not allow you to run this "
"program. Contact the system administrator."
msgstr ""
"El mecanismu d'autorización soyacente (sudo) nun permite executar esti "
"programa. Contaute col alministrador del sistema."

#: ../libgksu/libgksu.c:2843
#, c-format
msgid "sudo terminated with %d status"
msgstr "sudo terminó col estáu %d"

#: ../libgksu/libgksu-dialog.c:162
msgid "<b>You have capslock on</b>"
msgstr "<b>Tienes la tecla capslock activa</b>"

#: ../libgksu/libgksu-dialog.c:267
msgid "Remember password"
msgstr "Remembrar contraseña"

#: ../libgksu/libgksu-dialog.c:297
msgid "Save for this session"
msgstr "Guardar pa esta sesión"

#: ../libgksu/libgksu-dialog.c:305
msgid "Save in the keyring"
msgstr "Guardar nel aniellu de claves"

#: ../libgksu/libgksu-dialog.c:387
msgid "_Cancel"
msgstr "_Encaboxar"

#: ../libgksu/libgksu-dialog.c:394
msgid "_OK"
msgstr "_Aceutar"

#. label
#: ../libgksu/libgksu-dialog.c:434
msgid "<span weight=\"bold\" size=\"larger\">Type the root password.</span>\n"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Introduz la clave de root.</span>\n"

#. entry label
#: ../libgksu/libgksu-dialog.c:474
msgid "Password:"
msgstr "Contraseña:"

#: ../libgksu/libgksu-dialog.c:549
msgid "Granting Rights"
msgstr "Concediendo privilexos"
