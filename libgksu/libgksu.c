/*
 * libgksu -- a library providing access to su functionality
 * Copyright (C) 2004-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 * Portions Copyright (C) 2009 VMware, Inc.
 * Portions Copyright (C) 2010 Matthias Clasen <mclasen@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <https://gnu.org/licenses>.
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pty.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <errno.h>

#include <glibtop.h>
#include <glibtop/procstate.h>

#include <glib/gstdio.h>
#include <gio/gio.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#ifdef HAVE_INTROSPECTION
#include <girepository.h>
#endif

#ifdef ENABLE_KEYRING
#include <libsecret/secret.h>
#endif

#include "defines.h"

#include "libgksu.h"
#include "libgksu-dialog.h"

struct _GksuContextPrivate {
  /* Xauth stuff. */
  char *xauth;
  char *dir;
  char *display;

  gboolean sudo_mode;

  GSettings *gsettings;

  /* What to run, with whose powers. */
  char *user;
  char *command;

  gboolean login_shell;
  gboolean keep_env;

  /* UI options. */
  char *description;
  char *message;
  char *alert;
  gboolean grab;
  gboolean always_ask_password;

  /* Reference counting. */
  int ref_count;

  gboolean debug;
};

GType
gksu_error_get_type (void)
{
  static GType etype = 0;
  if (etype == 0) {
    static const GEnumValue values[] = {
      { GKSU_ERROR_HELPER, "GKSU_ERROR_HELPER", "helper" },
      { GKSU_ERROR_NOCOMMAND, "GKSU_ERROR_NOCOMMAND", "nocommand" },
      { GKSU_ERROR_NOPASSWORD, "GKSU_ERROR_NOPASSWORD", "nopassword" },
      { GKSU_ERROR_FORK, "GKSU_ERROR_FORK", "fork" },
      { GKSU_ERROR_EXEC, "GKSU_ERROR_EXEC", "exec" },
      { GKSU_ERROR_PIPE, "GKSU_ERROR_PIPE", "pipe" },
      { GKSU_ERROR_PIPEREAD, "GKSU_ERROR_PIPEREAD", "piperead" },
      { GKSU_ERROR_WRONGPASS, "GKSU_ERROR_WRONGPASS", "wrongpass" },
      { GKSU_ERROR_CHILDFAILED, "GKSU_ERROR_CHILDFAILED", "childfailed" },
      { GKSU_ERROR_CANCELED, "GKSU_ERROR_CANCELED", "canceled" },
      { GKSU_ERROR_WRONGAUTOPASS, "GKSU_ERROR_WRONGAUTOPASS", "wrongautopass" },
      { 0, NULL, NULL }
    };
    etype = g_enum_register_static ("GksuError", values);
  }
  return etype;
}

static pid_t
test_lock (const char *fname)
{
   int FD = g_open (fname, 0);
   if (FD < 0) {
      if (errno == ENOENT)
        {
          /* The file does not exist. */
          return 0;
        }
        else
        {
          perror ("open");
          return (-1);
        }
   }
   struct flock fl;
   fl.l_type = F_WRLCK;
   fl.l_whence = SEEK_SET;
   fl.l_start = 0;
   fl.l_len = 0;
   if (fcntl (FD, F_GETLK, &fl) < 0)
     {
       g_critical ("fcntl error");
       close (FD);
       return (-1);
     }
   close (FD);
   /* Lock is available. */
   if (fl.l_type == F_UNLCK)
      return (0);
   /* The file is locked by another process */
   return (fl.l_pid);
}

static int
get_lock (const char *File)
{
   int FD = g_open (File, O_RDWR | O_CREAT | O_TRUNC, 0640);
   if (FD < 0)
   {
      /* Read only... can't have locking problems there. */
      if (errno == EROFS)
      {
	 g_warning(_("Not using locking for read only lock file %s"),File);
         /* Need something for the caller to close. */
         return dup (0);
      }

      /* Feh... We do this to distinguish the lock vs open case. */
      errno = EPERM;
      return -1;
   }
   fcntl (FD,F_SETFD, FD_CLOEXEC);

   /* Aquire a write lock. */
   struct flock fl;
   fl.l_type = F_WRLCK;
   fl.l_whence = SEEK_SET;
   fl.l_start = 0;
   fl.l_len = 0;
   if (fcntl(FD,F_SETLK,&fl) == -1)
   {
      if (errno == ENOLCK)
      {
         g_warning (_("Not using locking for nfs mounted lock file %s"), File);
         g_unlink (File);
         close (FD);
         /* Need something for the caller to close. */
         return dup (0);
      }

      int Tmp = errno;
      close (FD);
      errno = Tmp;
      return -1;
   }

   return FD;
}

/*
 * code 'stolen' from gnome-session's logout.c
 *
 * Originally written by Owen Taylor <otaylor@redhat.com>
 * Copyright (C) Red Hat
 */
typedef struct {
#if GTK_CHECK_VERSION (3, 22, 0)
  GdkMonitor   *monitor;
#else
  int           monitor;
#endif
  GdkRectangle  area;
  int           rowstride;
  GtkWidget    *draw_widget;
  cairo_surface_t *draw_surface;
  cairo_surface_t *start_is, *end_is, *frame_is;
  GTimeVal      start_time;
  unsigned int  fadeout_thread_id;
} FadeoutData;

static GList *fade_data_list = NULL;

#define FADE_DURATION 500.0

static void
get_current_frame (FadeoutData *fadeout,
                   double       sat)
{
  unsigned char *sp, *ep, *fp;
  int i, j, width, offset;

  width = fadeout->area.width * 4;
  offset = 0;

  cairo_surface_flush (fadeout->start_is);
  cairo_surface_flush (fadeout->end_is);
  for (i = 0; i < fadeout->area.height; i++)
    {
      sp = cairo_image_surface_get_data (fadeout->start_is) + offset;
      ep = cairo_image_surface_get_data (fadeout->end_is)   + offset;
      fp = cairo_image_surface_get_data (fadeout->frame_is) + offset;

      for (j = 0; j < width; j += 4)
	{
	  unsigned char a = abs (*(sp++) - ep[0]);
	  unsigned char r = abs (*(sp++) - ep[1]);
	  unsigned char g = abs (*(sp++) - ep[2]);
	  unsigned char b = abs (*(sp++) - ep[3]);

	  *(fp++) = *(ep++) + a * sat;
	  *(fp++) = *(ep++) + r * sat;
	  *(fp++) = *(ep++) + g * sat;
	  *(fp++) = *(ep++) + b * sat;
	}

      offset += cairo_image_surface_get_stride (fadeout->frame_is);
    }
  cairo_surface_mark_dirty (fadeout->frame_is);
}

static void
darken_image_surface (cairo_surface_t *is)
{
  int width, height, rowstride;
  int i, j;
  unsigned char *p, *pixels;

  width     = cairo_image_surface_get_width (is) * 4;
  height    = cairo_image_surface_get_height (is);
  rowstride = cairo_image_surface_get_stride (is);
  pixels    = cairo_image_surface_get_data (is);

  cairo_surface_flush (is);
  for (i = 0; i < height; i++)
    {
      p = pixels + (i * rowstride);
      for (j = 0; j < width; j++)
	p [j] >>= 1;
    }
  cairo_surface_mark_dirty (is);
}

static gboolean
fadeout_callback (FadeoutData *fadeout)
{
  GTimeVal current_time;
  double elapsed, percent;

  g_get_current_time (&current_time);
  elapsed = ((((double)current_time.tv_sec - fadeout->start_time.tv_sec) * G_USEC_PER_SEC +
	      (current_time.tv_usec - fadeout->start_time.tv_usec))) / 1000.0;

  if (elapsed < 0)
    {
      g_warning ("System clock seemed to go backwards?");
      elapsed = G_MAXDOUBLE;
    }

  percent = CLAMP(1.0 - elapsed / FADE_DURATION, 0.0, 1.0);
  get_current_frame (fadeout, percent);

  cairo_surface_destroy (fadeout->draw_surface);
  fadeout->draw_surface = cairo_surface_reference (fadeout->frame_is);
  gtk_widget_queue_draw (fadeout->draw_widget);
  gdk_display_flush (gtk_widget_get_display (fadeout->draw_widget));

  if (elapsed <= FADE_DURATION)
    return TRUE;
  else
    {
      fadeout->fadeout_thread_id = 0;
      return FALSE;
    }
}

static void
fadeout_free (FadeoutData *fadeout)
{
  g_assert (fadeout != NULL);

  gtk_widget_hide (fadeout->draw_widget);
  gtk_widget_destroy (fadeout->draw_widget);

  cairo_surface_destroy (fadeout->start_is);
  cairo_surface_destroy (fadeout->end_is);
  cairo_surface_destroy (fadeout->frame_is);
  cairo_surface_destroy (fadeout->draw_surface);

  fade_data_list = g_list_remove (fade_data_list, (gconstpointer) fadeout);
  g_free (fadeout);
}

static gboolean
fadein_callback (FadeoutData *fadeout)
{
  GTimeVal current_time;
  double elapsed, percent;

  g_get_current_time (&current_time);
  elapsed = ((((double)current_time.tv_sec - fadeout->start_time.tv_sec) * G_USEC_PER_SEC +
	      (current_time.tv_usec - fadeout->start_time.tv_usec))) / 1000.0;

  if (elapsed < 0)
    {
      g_warning ("System clock seemed to go backwards?");
      elapsed = G_MAXDOUBLE;
    }

  percent = CLAMP(elapsed / FADE_DURATION, 0.0, 1.0);
  get_current_frame (fadeout, percent);

  cairo_surface_destroy (fadeout->draw_surface);
  fadeout->draw_surface = cairo_surface_reference (fadeout->frame_is);
  gtk_widget_queue_draw (fadeout->draw_widget);
  gdk_display_flush (gtk_widget_get_display (fadeout->draw_widget));

  if (elapsed <= FADE_DURATION)
    return TRUE;
  else
    return FALSE;
}

static gboolean
draw_callback (GtkWidget *widget,
#if GTK_CHECK_VERSION (3, 0, 0)
               cairo_t   *cr,
#else
               GdkEventExpose *event,
#endif
               gpointer   data)
{
  FadeoutData *fadeout = (FadeoutData *) data;
#if !GTK_CHECK_VERSION (3, 0, 0)
  GdkWindow *window;
  cairo_t *cr;

  window = gtk_widget_get_window (widget);
  cr = gdk_cairo_create (window);

  gdk_cairo_rectangle (cr, &event->area);
  cairo_clip (cr);
#endif
  cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
  cairo_pattern_set_extend (cairo_get_source (cr), CAIRO_EXTEND_REPEAT);

  if (fadeout->draw_surface != NULL &&
      cairo_surface_get_reference_count (fadeout->draw_surface) > 0)
  {
    cairo_set_source_surface (cr, fadeout->draw_surface, 0, 0);
    cairo_paint (cr);
  }

#if !GTK_CHECK_VERSION (3, 0, 0)
  cairo_destroy (cr);
#endif
  return FALSE;
}

static void
#if GTK_CHECK_VERSION (3, 22, 0)
fadeout_monitor (GdkMonitor *monitor)
#else
fadeout_monitor (int monitor)
#endif
{
  FadeoutData *fadeout;
  GdkScreen *screen;
  GdkWindow *root_window;
  cairo_t *cr;

  fadeout = g_new (FadeoutData, 1);

  fadeout->monitor = monitor;
  fadeout->draw_widget = GTK_WIDGET(gtk_window_new (GTK_WINDOW_POPUP));

  screen = gtk_widget_get_screen (fadeout->draw_widget);
  root_window = gdk_screen_get_root_window (screen);

#if GTK_CHECK_VERSION (3, 22, 0)
  gdk_monitor_get_geometry (monitor, &fadeout->area);
#else
  gdk_screen_get_monitor_geometry (screen, monitor, &fadeout->area);
#endif

  gtk_window_move (GTK_WINDOW(fadeout->draw_widget),
                   fadeout->area.x, fadeout->area.y);
  gtk_widget_set_size_request (fadeout->draw_widget,
                               fadeout->area.width, fadeout->area.height);
  gtk_window_set_resizable (GTK_WINDOW(fadeout->draw_widget), FALSE);
#if !GTK_CHECK_VERSION (3, 0, 0)
  gtk_widget_set_colormap (fadeout->draw_widget,
                           gdk_screen_get_system_colormap (screen));
#endif
  gtk_widget_set_visual (fadeout->draw_widget,
                         gdk_screen_get_system_visual (screen));
  gtk_widget_set_app_paintable (fadeout->draw_widget, TRUE);

#if GTK_CHECK_VERSION (3, 0, 0)
  g_signal_connect (G_OBJECT(fadeout->draw_widget),
                    "draw",
                    G_CALLBACK(draw_callback),
                    (gpointer) fadeout);
#else
  g_signal_connect (G_OBJECT(fadeout->draw_widget),
                    "expose-event",
                    G_CALLBACK(draw_callback),
                    (gpointer) fadeout);
#endif

#if GTK_CHECK_VERSION(3, 10, 0)
  fadeout->start_is =
    gdk_window_create_similar_image_surface (root_window,
                                             CAIRO_FORMAT_RGB24,
                                             fadeout->area.width,
                                             fadeout->area.height,
                                             0);
#else
  fadeout->start_is = cairo_image_surface_create (CAIRO_FORMAT_RGB24,
                                                  fadeout->area.width,
                                                  fadeout->area.height);
#endif
  fadeout->end_is = cairo_surface_create_similar (fadeout->start_is,
                                                  CAIRO_CONTENT_COLOR,
                                                  fadeout->area.width,
                                                  fadeout->area.height);
  fadeout->frame_is = cairo_surface_create_similar (fadeout->start_is,
                                                    CAIRO_CONTENT_COLOR,
                                                    fadeout->area.width,
                                                    fadeout->area.height);

  cr = cairo_create (fadeout->start_is);
  cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
  gdk_cairo_set_source_window (cr, root_window,
                               -fadeout->area.x, -fadeout->area.y);
  cairo_paint (cr);
  cairo_destroy (cr);

  cr = cairo_create (fadeout->end_is);
  cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
  cairo_set_source_surface (cr, fadeout->start_is, 0, 0);
  cairo_paint (cr);
  cairo_destroy (cr);
  darken_image_surface (fadeout->end_is);

  /* Fill the frame image surface with the starting surface. */
  get_current_frame (fadeout, 1.0);

  gtk_widget_show_all (fadeout->draw_widget);
#if !GTK_CHECK_VERSION (3, 0, 0)
  gdk_window_set_back_pixmap (gtk_widget_get_window (fadeout->draw_widget),
                              NULL, FALSE);
#endif

  fadeout->draw_surface = cairo_surface_reference (fadeout->frame_is);
  gtk_widget_queue_draw (fadeout->draw_widget);
  gdk_display_flush (gtk_widget_get_display (fadeout->draw_widget));

  g_get_current_time (&fadeout->start_time);
  fadeout->fadeout_thread_id =
    gdk_threads_add_idle ((GSourceFunc) fadeout_callback, fadeout);

  fade_data_list = g_list_prepend (fade_data_list, (gpointer) fadeout);
}

/* End of "stolen" code */

/**
 * gksu_major_version:
 *
 * Returns the major version number of the gksu library.
 * (e.g. in libgksu version 3.4.2 this is 3.)
 *
 * This function is in the library, so it represents the gksu
 * library your code is running against. Contrast with the
 * #GKSU_MAJOR_VERSION macro, which represents the major version of
 * the libgksu headers you have included when compiling your code.
 *
 * Returns: the major version number of the gksu library
 */
unsigned int
gksu_major_version (void)
{
  return GKSU_MAJOR_VERSION;
}

/**
 * gksu_minor_version:
 *
 * Returns the minor version number of the gksu library.
 * (e.g. in libgksu version 3.4.2 this is 4.)
 *
 * This function is in the library, so it represents the gksu
 * library your code is are running against. Contrast with the
 * #GKSU_MINOR_VERSION macro, which represents the minor version of
 * the libgksu headers you have included when compiling your code.
 *
 * Returns: the minor version number of the gksu library
 */
unsigned int
gksu_minor_version (void)
{
  return GKSU_MINOR_VERSION;
}

/**
 * gksu_micro_version:
 *
 * Returns the micro version number of the gksu library.
 * (e.g. in libgksu version 3.4.2 this is 2.)
 *
 * This function is in the library, so it represents the gksu
 * library your code is are running against. Contrast with the
 * #GKSU_MICRO_VERSION macro, which represents the micro version of
 * the libgksu headers you have included when compiling your code.
 *
 * Returns: the micro version number of the gksu library
 */
unsigned int
gksu_micro_version (void)
{
  return GKSU_MICRO_VERSION;
}

/**
 * gksu_binary_age:
 *
 * Returns the binary age as passed to `libtool` when building the
 * gksu library the process is running against.
 * If `libtool` means nothing to you, don't worry about it.
 *
 * Returns: the binary age of the gksu library
 */
unsigned int
gksu_binary_age (void)
{
  return GKSU_BINARY_AGE;
}

/**
 * gksu_interface_age:
 *
 * Returns the interface age as passed to `libtool` when building
 * the gksu library the process is running against.
 * If `libtool` means nothing to you, don't worry about it.
 *
 * Returns: the interface age of the gksu library
 */
unsigned int
gksu_interface_age (void)
{
  return GKSU_INTERFACE_AGE;
}

/**
 * gksu_check_version:
 * @major: the major version to check
 * @minor: the minor version to check
 * @micro: the micro version to check
 *
 * Checks if the gksu library in use is compatible with the given
 * version.
 *
 * Returns: TRUE if compatible, FALSE if not
 */
gboolean
gksu_check_version (unsigned int major, unsigned int minor, unsigned int micro)
{
  return GKSU_CHECK_VERSION (major, minor, micro) ? TRUE : FALSE;
}

#define GRAB_TRIES	32
#define GRAB_WAIT	250 /* milliseconds */

void
report_failed_grab (void)
{
  GtkWidget *dialog;

  dialog = g_object_new (GTK_TYPE_MESSAGE_DIALOG,
			 "message-type", GTK_MESSAGE_WARNING,
			 "buttons", GTK_BUTTONS_CLOSE,
			 NULL);

  gtk_message_dialog_set_markup (GTK_MESSAGE_DIALOG(dialog),
                                 _("<b><big>Could not grab your keyboard or mouse.</big></b>"
                                 "\n\n"
                                 "A malicious client may be eavesdropping on "
                                 "your session or you may have just clicked a "
                                 "menu or some application just decided to "
                                 "get focus."
                                 "\n\n"
                                 "Try again."));

  gtk_window_set_keep_above (GTK_WINDOW(dialog), TRUE);
  gtk_dialog_run (GTK_DIALOG(dialog));
  gtk_widget_destroy (dialog);

  while (gtk_events_pending ())
    gtk_main_iteration ();

}

static void
ungrab_keyboard_and_pointer (GdkDisplay *display)
{
#if GTK_CHECK_VERSION (3, 20, 0)
  GdkSeat *seat;

  seat = gdk_display_get_default_seat (display);

  gdk_seat_ungrab (seat);
  gdk_display_flush (display);
#else
  gdk_keyboard_ungrab (GDK_CURRENT_TIME);
  gdk_pointer_ungrab (GDK_CURRENT_TIME);
  gdk_flush ();
#endif
}

static void
unlock_screen (GdkDisplay *display,
               int         lock)
{
  GList *list;

  ungrab_keyboard_and_pointer (display);

  for (list = g_list_last (fade_data_list); list != NULL; list = list->prev)
    {
      FadeoutData *fade_data = (FadeoutData *) list->data;
      /* In case fadeout_callback has already started. */
      if (fade_data->fadeout_thread_id > 0)
        g_source_remove (fade_data->fadeout_thread_id);
      fade_data->fadeout_thread_id = 0;

      g_get_current_time (&fade_data->start_time);
      gdk_threads_add_idle ((GSourceFunc) fadein_callback, fade_data);
    }

  while (gtk_events_pending ())
    gtk_main_iteration ();

  g_list_foreach (fade_data_list, (GFunc) fadeout_free, NULL);

  close (lock);
}

#if GTK_CHECK_VERSION (3, 20, 0)
static void
prepare_window_grab_cb (GdkSeat   *seat,
                        GdkWindow *window,
                        gpointer   data)
{
  gdk_window_show_unraised (window);
}
#endif

static gboolean
grab_keyboard_and_pointer (GdkDisplay *display,
                           GtkWidget  *widget)
{
  GdkGrabStatus status;
#if GTK_CHECK_VERSION (3, 20, 0)
  GdkSeat *seat;
#else
  GdkGrabStatus pstatus;
#endif
  GdkCursor *cursor;
  gint grab_tries = 0;

#if GTK_CHECK_VERSION (3, 20, 0)
  seat = gdk_display_get_default_seat (display);
#endif
  cursor = gdk_cursor_new_for_display (display, GDK_LEFT_PTR);

  while (TRUE)
    {
#if GTK_CHECK_VERSION (3, 20, 0)
      status = gdk_seat_grab (seat, gtk_widget_get_window (widget),
                              GDK_SEAT_CAPABILITY_ALL,
                              TRUE, cursor, NULL,
                              prepare_window_grab_cb, NULL);

      if (status == GDK_GRAB_SUCCESS)
        {
          break;
        }
#else
      gdk_window_set_cursor (gtk_widget_get_window (widget), cursor);
      status = gdk_keyboard_grab (gtk_widget_get_window (widget),
                                  FALSE, GDK_CURRENT_TIME);
      pstatus = gdk_pointer_grab (gtk_widget_get_window (widget),
                                  TRUE, 0, NULL, NULL, GDK_CURRENT_TIME);

      if (status == GDK_GRAB_SUCCESS && pstatus == GDK_GRAB_SUCCESS)
        {
          break;
        }
#endif

      g_usleep (GRAB_WAIT * 1000);

      if (++grab_tries > GRAB_TRIES)
	{
          gtk_widget_hide (widget);
          ungrab_keyboard_and_pointer (display);
#if GTK_CHECK_VERSION (3, 0, 0)
          g_object_unref (cursor);
#else
          gdk_cursor_unref (cursor);
#endif
          return FALSE;
	}
    }

#if GTK_CHECK_VERSION (3, 0, 0)
  g_object_unref (cursor);
#else
  gdk_cursor_unref (cursor);
#endif
  return TRUE;
}

static int
lock_screen (GdkDisplay *display,
             GtkWidget  *dialog)
{
#if !GTK_CHECK_VERSION (3, 22, 0)
  GdkScreen *screen;
#endif
  int lock = -1;
  int i = 0;
  pid_t pid;

  char *fname = g_strdup (g_getenv ("GKSU_LOCK_FILE"));
  if (fname == NULL)
    fname = g_strdup_printf ("%s/.gksu.lock", g_get_home_dir ());

  pid = test_lock (fname);

  if (pid != 0)
    {
      g_warning ("Lock taken by pid: %i. Exiting.", pid);
      exit (0);
    }

  lock = get_lock(fname);
  if (lock < 0)
    g_warning ("Unable to create lock file.");
  g_free (fname);

#if GTK_CHECK_VERSION (3, 22, 0)
  for (i = 0; i < gdk_display_get_n_monitors (display); ++i)
    fadeout_monitor (gdk_display_get_monitor (display, i));
#else
  screen = gtk_widget_get_screen (dialog);

  for (i = 0; i < gdk_screen_get_n_monitors (screen); ++i)
    fadeout_monitor (i);
#endif
  gtk_widget_show_all (dialog);

  if (!grab_keyboard_and_pointer (display, dialog))
    {
      unlock_screen (display, lock);
      report_failed_grab ();
      exit (1);
    }

  while (gtk_events_pending ())
    gtk_main_iteration ();

  return lock;
}

#ifdef ENABLE_KEYRING
static const SecretSchema keyring_schema = {
  "org.gksu.password", SECRET_SCHEMA_DONT_MATCH_NAME,
  {
    { "user",  SECRET_SCHEMA_ATTRIBUTE_STRING },
    { "type", SECRET_SCHEMA_ATTRIBUTE_STRING },
    { "creator", SECRET_SCHEMA_ATTRIBUTE_STRING },
    { NULL, 0 },
  }
};

static char *
get_libsecret_password (GksuContext *context)
{
  char *found;
  char *passwd = NULL;

  found = secret_password_lookup_sync (&keyring_schema, NULL, NULL,
                                       "user", gksu_context_get_user (context),
                                       "type", "local",
                                       "creator", "gksu",
                                       NULL);

  if (found != NULL)
    {
      int passwd_length = strlen (found);

      passwd = g_locale_from_utf8 (found, passwd_length,
				   NULL, NULL, NULL);
      if (passwd != NULL)
        {
          passwd_length = strlen (passwd);

          if (passwd[passwd_length - 1] == '\n')
            passwd[passwd_length - 1] = '\0';
        }
    }

  return passwd;
}

static void
keyring_create_item_cb (GObject *source_object,
                        GAsyncResult *result,
                        gpointer keyring_loop)
{
  g_main_loop_quit (keyring_loop);
}

static void
set_libsecret_password (GksuContext *context, const char *password)
{
  GSettings *gsettings;
  gboolean save_to_keyring;

  gsettings = context->priv->gsettings;
  save_to_keyring = g_settings_get_boolean (gsettings, "save-to-keyring");

  if (password && save_to_keyring)
    {
      static GMainLoop *keyring_loop = NULL;

      char *keyring_name;
      char *key_name;

      key_name = g_strdup_printf ("Local password for user %s",
				  gksu_context_get_user (context));

      keyring_loop = g_main_loop_new (NULL, FALSE);

      keyring_name = g_settings_get_string (gsettings, "save-keyring");
      if (keyring_name == NULL)
        keyring_name = g_strdup (SECRET_COLLECTION_SESSION);

      secret_password_store (&keyring_schema, keyring_name,
                             key_name, password, NULL,
                             keyring_create_item_cb, keyring_loop,
                             "user", gksu_context_get_user (context),
                             "type", "local",
                             "creator", "gksu",
                             NULL);

      g_main_loop_run (keyring_loop);
      g_free (keyring_name);
  }
}

static void
unset_libsecret_password (GksuContext *context)
{
  GSettings *gsettings;
  gboolean save_to_keyring;

  gsettings = context->priv->gsettings;
  save_to_keyring = g_settings_get_boolean (gsettings, "save-to-keyring");

  if (save_to_keyring)
    {
      secret_password_clear_sync (&keyring_schema, NULL, NULL,
                                  "user", gksu_context_get_user (context),
                                  "type", "local",
                                  "creator", "gksu",
                                  NULL);
    }
}
#endif

void
get_configuration_options (GksuContext *context)
{
  GSettings *gsettings = context->priv->gsettings;
  gboolean force_grab;

  context->priv->grab = !g_settings_get_boolean (gsettings, "disable-grab");
  force_grab = g_settings_get_boolean (gsettings, "force-grab");
  if (force_grab)
    context->priv->grab = TRUE;

  context->priv->sudo_mode = g_settings_get_boolean (gsettings, "sudo-mode");
}

/**
 * su_ask_password:
 * @context: a #GksuContext
 * @prompt: (nullable): the prompt that should be used instead of
 * "Password: "
 * @data: data that is passed by gksu_*_full()
 * @error: a pointer to pointer #GError that will be filled with
 * data if an error happens.
 *
 * This is a convenience function to create a #GksuuiDialog and
 * request the password.
 *
 * Returns: (transfer full): a newly allocated string containing
 * the password or NULL if an error happens or the user cancels
 * the action
 */
static char *
su_ask_password (GksuContext *context, const char *prompt,
		 gpointer data, GError **error)
{
  GtkWidget *dialog = NULL;
  GdkDisplay *display = NULL;
  char *msg;
  char *password = NULL, *tmp = NULL;
  int retvalue = 0;
  int lock = 0;
  GQuark gksu_quark;

  gksu_quark = g_quark_from_string (PACKAGE);

  dialog = gksu_dialog_new (context->priv->grab, context->priv->sudo_mode);

  display = gtk_widget_get_display (dialog);

  if (prompt)
    gksu_dialog_set_prompt (GKSU_DIALOG(dialog), _(prompt));

  if (context->priv->message != NULL)
    gksu_dialog_set_message (GKSU_DIALOG(dialog), context->priv->message);
  else
    {
      char *command = NULL;
      if (context->priv->description != NULL)
	command = context->priv->description;
      else
	command = context->priv->command;

      if (context->priv->sudo_mode)
	{
	  if (g_strcmp0(context->priv->user, "root") == 0)
	    msg = g_strdup_printf (_("<b><big>Enter your password to perform"
				     " administrative tasks</big></b>\n\n"
				     "The application '%s' lets you "
				     "modify essential parts of your "
				     "system."),
				   g_markup_escape_text(command, -1));
	  else
	    msg = g_strdup_printf (_("<b><big>Enter your password to run "
				     "the application '%s' as user %s"
				     "</big></b>"),
				   g_markup_escape_text(command, -1),
				   context->priv->user);
	}
      else
	{
        if (strcmp(gksu_context_get_user (context), "root") == 0)
          msg = g_strdup_printf (_("<b><big>Enter the administrative password"
                                   "</big></b>\n\n"
                                   "The application '%s' lets you "
                                   "modify essential parts of your "
                                   "system."),
				   command);
        else
          msg = g_strdup_printf (_("<b><big>Enter the password of %s to run "
                                   "the application '%s'"
                                   "</big></b>"),
				   context->priv->user, command);
      }

      gksu_dialog_set_message (GKSU_DIALOG(dialog), msg);
      g_free (msg);
    }

  if (context->priv->alert != NULL)
    gksu_dialog_set_alert (GKSU_DIALOG(dialog), context->priv->alert);

  if (context->priv->grab)
    {
      lock = lock_screen (display, dialog);
    }
  retvalue = gtk_dialog_run (GTK_DIALOG(dialog));

  if (retvalue == GTK_RESPONSE_OK)
    {
      tmp = gksu_dialog_get_password (GKSU_DIALOG(dialog));
      password = g_locale_from_utf8 (tmp, strlen (tmp), NULL, NULL, NULL);
      g_free (tmp);
    }
  else
    password = NULL;

  gtk_widget_hide (dialog);
  gtk_widget_destroy (dialog);

  if (context->priv->grab)
    {
      unlock_screen (display, lock);
    }

  switch (retvalue)
    {
      case GTK_RESPONSE_CANCEL:
      case GTK_RESPONSE_DELETE_EVENT:
        g_set_error (error, gksu_quark,
                     GKSU_ERROR_CANCELED,
                     _("Password prompt canceled."));
        break;
      default:
        break;
    }

  while (gtk_events_pending ())
    gtk_main_iteration ();

  return password;
}

static void
cb_toggled_cb (GtkWidget *button, gpointer data)
{
  GSettings *gsettings;
  char *key;
  gboolean toggled;
  char *key_name;

  g_return_if_fail (data != NULL);

  key_name = (char *) data;

  gsettings = g_settings_new (GSETTINGS_SCHEMA);
  toggled = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(button));

  key = g_strdup_printf ("%s", key_name);

  if (g_strcmp0 (key_name, "display-no-pass-info") == 0)
    {
      /* the meaning of the key is the exact opposite of the meaning
	 of the answer - when the check button is checked the key must
	 be off
      */
      g_settings_set_boolean (gsettings, key, !toggled);
    }
  else
    g_settings_set_boolean (gsettings, key, toggled);

  g_object_unref (gsettings);

  g_free (key);
}

void
no_pass (GksuContext *context, gpointer data)
{
  GtkWidget *dialog;
  GtkWidget *dialog_msg_area;
  GtkWidget *check_button;

  char *command = NULL;

  if (context->priv->description != NULL)
    command = context->priv->description;
  else
    command = context->priv->command;

  dialog = gtk_message_dialog_new_with_markup (NULL, 0,
					       GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE,
					       _("<b><big>Granted permissions without asking "
						 "for password</big></b>"
						 "\n\n"
						 "The '%s' program was started with "
						 "the privileges of the %s user without "
						 "the need to ask for a password, due to "
						 "your system's authentication mechanism "
						 "setup."
						 "\n\n"
						 "It is possible that you are being allowed "
						 "to run specific programs as user %s "
						 "without the need for a password, or that "
						 "the password is cached."
						 "\n\n"
						 "This is not a problem report; it's "
						 "simply a notification to make sure "
						 "you are aware of this."),
					       command,
					       context->priv->user,
					       context->priv->user);

  check_button = gtk_check_button_new_with_mnemonic (_("Do _not display this message again"));
  g_signal_connect (G_OBJECT(check_button), "toggled",
		    G_CALLBACK(cb_toggled_cb), "display-no-pass-info");

  dialog_msg_area =
    gtk_message_dialog_get_message_area (GTK_MESSAGE_DIALOG(dialog));
  gtk_box_pack_start (GTK_BOX(dialog_msg_area), check_button, TRUE, TRUE, 2);

  gtk_widget_show_all (dialog);
  gtk_dialog_run (GTK_DIALOG(dialog));
  gtk_widget_destroy (GTK_WIDGET(dialog));

  while (gtk_events_pending ())
    gtk_main_iteration ();
}

static void
gksu_prompt_grab (GksuContext *context)
{
  GtkWidget *d;

  d = gtk_message_dialog_new_with_markup (NULL, 0, GTK_MESSAGE_QUESTION,
					  GTK_BUTTONS_YES_NO,
					  _("<b>Would you like your screen to be \"grabbed\"\n"
					    "while you enter the password?</b>"
					    "\n\n"
					    "This means all applications will be paused to avoid\n"
					    "the eavesdropping of your password by a a malicious\n"
					    "application while you type it."));

  if (gtk_dialog_run (GTK_DIALOG(d)) == GTK_RESPONSE_NO)
    gksu_context_set_grab (context, FALSE);
  else
    gksu_context_set_grab (context, TRUE);

  gtk_widget_destroy (d);
}

static void
nullify_password (char **pass)
{
  if (pass != NULL && *pass != NULL)
    {
      memset (*pass, 0, strlen (*pass));
      g_free (*pass);
    }
  *pass = NULL;
  pass = NULL;
}

static void
read_line (int fd, char *buffer, int n)
{
  int counter;
  char tmp[2] = {0};

  for (counter = 0; counter < (n - 1); counter++)
    {
      tmp[0] = '\0';
      read (fd, tmp, 1);
      if (tmp[0] == '\n')
        break;
      buffer[counter] = tmp[0];
    }
  buffer[counter] = '\0';
}

static char *
get_process_name (pid_t pid)
{
  static gboolean init;
  glibtop_proc_state buf;

  if (!init) {
    glibtop_init();
    init = TRUE;
  }

  glibtop_get_proc_state (&buf, pid);
  return g_strdup (buf.cmd);
}

static char *
get_xauth_token (GksuContext *context, char *display)
{
  char *xauth_bin, *tmp;
  FILE *xauth_output;
  char *xauth = g_new0 (char, 256);

  /* find out where the xauth binary is located */
  xauth_bin = g_find_program_in_path ("xauth");
  if (xauth_bin == NULL)
    {
      if (g_file_test ("/usr/bin/xauth", G_FILE_TEST_IS_EXECUTABLE))
        xauth_bin = g_strdup ("/usr/bin/xauth");
      else if (g_file_test ("/usr/X11R6/bin/xauth", G_FILE_TEST_IS_EXECUTABLE))
        xauth_bin = g_strdup ("/usr/X11R6/bin/xauth");
      else
        {
          g_fprintf (stderr,
                     "Failed to obtain xauth key: xauth binary not found "
                     "at usual locations");

          return NULL;
        }
    }

  /* get the authorization token */
  tmp = g_strdup_printf ("%s list %s | "
			 "head -1 | awk '{ print $3 }'",
			 xauth_bin,
			 display);
  if ((xauth_output = popen (tmp, "r")) == NULL)
    {
      g_fprintf (stderr,
                 "Failed to obtain xauth key: %s",
                 strerror (errno));
      return NULL;
    }
  fread (xauth, sizeof(char), 255, xauth_output);
  pclose (xauth_output);
  g_free (tmp);

  if (gksu_context_get_debug (context))
    {
      g_fprintf(stderr,
                "xauth: -%s-\n"
                "display: -%s-\n",
                xauth, display);
    }

  g_free (xauth_bin);
  return xauth;
}

/**
 * prepare_xauth:
 * @context: a #GksuContext
 *
 * Sets up the variables with values for the $DISPLAY environment
 * variable and xauth-related stuff. Also creates a temporary
 * directory to hold a .Xauthority.
 *
 * Returns: TRUE if it succeeds, FALSE if it fails.
 */
static gboolean
prepare_xauth (GksuContext *context)
{
  char *display;
  char *xauth;

  display = g_strdup (g_getenv ("DISPLAY"));
  xauth = get_xauth_token (context, display);
  if (xauth == NULL)
    {
      g_free (display);
      return FALSE;
    }

  /* If xauth is the empty string, then try striping the
   * hostname part of the DISPLAY string for getting the
   * auth token; this is needed for ssh-forwarded usage
   */
  if (!strcmp ("", xauth))
    {
      char *cut_display = NULL;

      g_free (xauth);
      cut_display = g_strdup (g_strrstr (display, ":"));
      xauth = get_xauth_token (context, cut_display);

      g_free (display);
      display = cut_display;
    }

  context->priv->xauth = xauth;
  context->priv->display = display;

  if (context->priv->debug)
    {
      g_fprintf(stderr,
                "final xauth: -%s-\n"
                "final display: -%s-\n",
                context->priv->xauth, context->priv->display);
    }

  return TRUE;
}

/* Write all of buf, even if write(2) is interrupted. */
static ssize_t
full_write (int d, const char *buf, size_t nbytes)
{
  ssize_t r, w = 0;

  /* Loop until nbytes of buf have been written. */
  while (w < nbytes) {
    /* Keep trying until write succeeds without interruption. */
    do {
      r = write(d, buf + w, nbytes - w);
    } while (r < 0 && errno == EINTR);

    if (r < 0) {
      return -1;
    }

    w += r;
  }

  return w;
}

static gboolean
copy (const char *fn, const char *dir)
{
  int in, out;
  int r;
  char *newfn;
  char buf[BUFSIZ] = "";

  newfn = g_strdup_printf("%s/.Xauthority", dir);

  out = g_open (newfn, O_WRONLY | O_CREAT | O_EXCL, 0600);
  if (out == -1)
    {
      if (errno == EEXIST)
        g_fprintf (stderr,
                   "Impossible to create the .Xauthority file: a file "
                   "already exists. This might be a security issue; "
                   "please investigate.");
      else
        g_fprintf (stderr,
                   "Error copying '%s' to '%s': %s",
                   fn, dir, strerror (errno));

      return FALSE;
    }

  in = g_open (fn, O_RDONLY);
  if (in == -1)
    {
      g_fprintf (stderr,
                 "Error copying '%s' to '%s': %s",
                 fn, dir, strerror (errno));
      return FALSE;
    }

  while ((r = read(in, buf, BUFSIZ)) > 0)
    {
      if (full_write(out, buf, r) == -1)
        {
          g_fprintf (stderr,
                     "Error copying '%s' to '%s': %s",
                     fn, dir, strerror (errno));
          return FALSE;
        }
    }

  if (r == -1)
    {
      g_fprintf (stderr,
                 "Error copying '%s' to '%s': %s",
                 fn, dir, strerror(errno));
      return FALSE;
    }

  return TRUE;
}

static gboolean
sudo_prepare_xauth (GksuContext *context)
{
  const char *template = PACKAGE "-XXXXXX";
  gboolean error_copying = FALSE;
  char *xauth = NULL;

  context->priv->dir = g_dir_make_tmp (template, NULL);
  if (context->priv->dir == NULL)
    {
      g_fprintf (stderr, "%s", strerror (errno));
      return FALSE;
    }

  xauth = g_strdup(g_getenv ("XAUTHORITY"));
  if (xauth == NULL)
    xauth = g_strdup_printf ("%s/.Xauthority", g_get_home_dir());

  error_copying = !copy (xauth, context->priv->dir);
  g_free (xauth);

  if (error_copying)
    return FALSE;

  return TRUE;
}

static void
sudo_reset_xauth (GksuContext *context)
{
  char *xauth = g_strdup_printf ("%s/.Xauthority", context->priv->dir);

  if (context->priv->debug)
    {
      g_fprintf (stderr, "xauth: %s\ndir: %s\n",
                 xauth, context->priv->dir);
    }

  g_unlink (xauth);
  g_rmdir (context->priv->dir);

  g_free (xauth);
}

/**
 * gksu_context_new
 *
 * This function should be used when creating a new #GksuContext to
 * pass to gksu_su_full() or gksu_sudo_full().
 * Free with gksu_context_unref() when done.
 *
 * Returns: (transfer full): a newly allocated #GksuContext
 */
GksuContext *
gksu_context_new (void)
{
  GksuContext *context;
  GksuContextPrivate *priv;

  /* Make sure we're using UTF-8 and getting our locale files from the right
     place. */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

  context = g_new (GksuContext, 1);

  priv = g_new (GksuContextPrivate, 1);
  context->priv = priv;

  priv->xauth = NULL;
  priv->dir = NULL;
  priv->display = NULL;

  priv->gsettings = g_settings_new (GSETTINGS_SCHEMA);

  priv->sudo_mode = FALSE;

  priv->user = g_strdup ("root");
  priv->command = NULL;

  priv->login_shell = FALSE;
  priv->keep_env = FALSE;

  priv->description = NULL;
  priv->message = NULL;
  priv->alert = NULL;
  priv->grab = TRUE;
  priv->always_ask_password = FALSE;

  priv->debug = FALSE;
  
  priv->ref_count = 1;

  get_configuration_options (context);

  return context;
}

/**
 * gksu_context_set_user:
 * @context: the #GksuContext you want to modify
 * @username: (not nullable): the target username
 *
 * Sets up what user the command will be run as. The default is
 * root, but you can run the command as any user.
 */
void
gksu_context_set_user (GksuContext *context, const char *username)
{
  g_return_if_fail (username != NULL);

  if (context->priv->user != NULL)
    g_free (context->priv->user);
  context->priv->user = g_strdup (username);
}

/**
 * gksu_context_get_user:
 * @context: the #GksuContext from which to grab the information
 *
 * Gets the user the command will be run as, as set by
 * gksu_context_set_user().
 *
 * Returns: (transfer none): a string with the user or NULL if not
   set
 */
const char *
gksu_context_get_user (GksuContext *context)
{
  return context->priv->user;
}

/**
 * gksu_context_set_command:
 * @context: the #GksuContext you want to modify
 * @command: (nullable): the command that shall be ran
 *
 * Sets up what command will run with the target user.
 */
void
gksu_context_set_command (GksuContext *context, const char *command)
{
  if (context->priv->command != NULL)
    g_free (context->priv->command);
  context->priv->command = command != NULL ? g_strdup (command) : NULL;
}

/**
 * gksu_context_get_command:
 * @context: the #GksuContext from which to grab the information
 *
 * Gets the command that will be run, as set by
 * gksu_context_set_command().
 *
 * Returns: (transfer none): a string with the command or NULL if
   not set
 */
const char *
gksu_context_get_command (GksuContext *context)
{
  return context->priv->command;
}

/**
 * gksu_context_set_login_shell:
 * @context: the #GksuContext you want to modify
 * @value: TRUE or FALSE
 *
 * Should the shell in which the command will be run be a login
 * shell?
 */
void
gksu_context_set_login_shell (GksuContext *context, gboolean value)
{
  context->priv->login_shell = value;
}

/**
 * gksu_context_get_login_shell:
 * @context: the #GksuContext from which to grab the information
 *
 * Finds out if the shell created by the underlying su process will
 * be a login shell.
 *
 * Returns: TRUE if the shell will be a login shell, FALSE otherwise
 */
gboolean
gksu_context_get_login_shell (GksuContext *context)
{
  return context->priv->login_shell;
}

/**
 * gksu_context_set_keep_env:
 * @context: the #GksuContext you want to modify
 * @value: TRUE or FALSE
 *
 * Should the environment be kept as it is? Defaults to TRUE.
 * Notice that setting this to FALSE may cause the X authorisation
 * stuff to fail.
 */
void
gksu_context_set_keep_env (GksuContext *context, gboolean value)
{
  context->priv->keep_env = value;
}

/**
 * gksu_context_get_keep_env:
 * @context: the #GksuContext from which to grab the information
 *
 * Finds out if the environment in which the application will be
 * run will be reset.
 *
 * Returns: TRUE if the environment is going to be kept, FALSE
 * otherwise
 */
gboolean
gksu_context_get_keep_env (GksuContext *context)
{
  return context->priv->keep_env;
}

/**
 * gksu_context_set_description:
 * @context: the #GksuContext you want to modify
 * @description: (nullable): a string to set the description for
 *
 * Set the nice name for the action that is being run that the
 * window that asks for the password will have. This is only meant
 * to be used if the default window is used, of course.
 */
void
gksu_context_set_description (GksuContext *context, const char *description)
{
  if (context->priv->description != NULL)
    g_free (context->priv->description);
  context->priv->description = description != NULL ? g_strdup (description) : NULL;
}

/**
 * gksu_context_get_description:
 * @context: the #GksuContext you want to get the description from
 *
 * Get the description that the window will have when the default
 * function for requesting the password is called.
 *
 * Returns: (transfer none): a string with the description or NULL
 * if not set
 */
const char *
gksu_context_get_description (GksuContext *context)
{
  return context->priv->description;
}

/**
 * gksu_context_set_message:
 * @context: the #GksuContext you want to modify
 * @message: (nullable): a string to set the message for
 *
 * Set the message that the window that asks for the password will
 * have. This is only meant to be used if the default window is
 * used, of course.
 */
void
gksu_context_set_message (GksuContext *context, const char *message)
{
  if (context->priv->message != NULL)
    g_free (context->priv->message);
  context->priv->message = message != NULL ? g_strdup (message) : NULL;
}

/**
 * gksu_context_get_message:
 * @context: the #GksuContext you want to get the message from.
 *
 * Get the message that the window will have when the default
 * function for requesting the password is called.
 *
 * Returns: (transfer none): a string with the message or NULL if
 * not set
 */
const char *
gksu_context_get_message (GksuContext *context)
{
  return context->priv->message;
}

/**
 * gksu_context_set_alert:
 * @context: the #GksuContext you want to modify
 * @alert: (nullable): a string to set the alert for
 *
 * Set the alert that the window that asks for the password will
 * have. This is only meant to be used if the default window is
 * used, of course. This alert should be used to display messages
 * such as "incorrect password", for instance.
 */
void
gksu_context_set_alert (GksuContext *context, const char *alert)
{
  if (context->priv->alert != NULL)
    g_free (context->priv->alert);
  context->priv->alert = alert != NULL ? g_strdup (alert) : NULL;
}

/**
 * gksu_context_get_alert:
 * @context: the #GksuContext you want to get the alert from
 *
 * Get the alert that the window will have when the default
 * function for requesting the password is called.
 *
 * Returns: (transfer none): a string with the alert or NULL if
 * not set
 */
const char *
gksu_context_get_alert (GksuContext *context)
{
  return context->priv->alert;
}

/**
 * gksu_context_set_grab:
 * @context: the #GksuContext you want to modify
 * @value: TRUE or FALSE
 *
 * Set up if debugging information should be printed.
 */
void
gksu_context_set_grab (GksuContext *context, gboolean value)
{
  context->priv->grab = value;
}

/**
 * gksu_context_get_grab:
 * @context: the #GksuContext you want to ask whether a grab will
 * be done
 *
 * Returns TRUE if gksu has been asked to do a grab on keyboard and
 * mouse when asking for the password.
 *
 * Returns: TRUE if yes, FALSE otherwise
 */
gboolean
gksu_context_get_grab (GksuContext *context)
{
  return context->priv->grab;
}

/**
 * gksu_context_set_always_ask_password:
 * @context: the #GksuContext you want to modify
 * @value: TRUE or FALSE
 *
 * Set up if gksu should always ask for a password. Notice that
 * this will only work when passwords are cached, as done by the
 * keyring for gksu's su mode and by sudo for gksu's sudo mode, but
 * will have no effect if su or sudo are set up to not require the
 * password at all.
 */
void
gksu_context_set_always_ask_password (GksuContext *context, gboolean value)
{
  context->priv->always_ask_password = value;
}

/**
 * gksu_context_get_always_ask_password:
 * @context: the #GksuContext you want to ask whether a grab will
 * be done.
 *
 * Returns TRUE if gksu has been asked to always ask for a password 
 * (even if sudo or the keyring have cached it)
 *
 * Returns: TRUE if yes, FALSE otherwise
 */
gboolean
gksu_context_get_always_ask_password (GksuContext *context)
{
   return context->priv->always_ask_password;
}

/**
 * gksu_context_set_debug:
 * @context: the #GksuContext you want to modify
 * @value: TRUE or FALSE
 *
 * Set up if debugging information should be printed.
 */
void
gksu_context_set_debug (GksuContext *context, gboolean value)
{
  context->priv->debug = value;
}

/**
 * gksu_context_get_debug:
 * @context: the #GksuContext from which to grab the information
 *
 * Finds out if the library is configured to print debugging
 * information.
 *
 * Returns: TRUE if it is, FALSE otherwise
 */
gboolean
gksu_context_get_debug (GksuContext *context)
{
  return context->priv->debug;
}

/**
 * gksu_context_free
 * @context: the #GksuContext to be freed
 *
 * Frees the given #GksuContext.
 */
void
gksu_context_free (GksuContext *context)
{
  GksuContextPrivate *priv = context->priv;

  g_free (priv->xauth);
  g_free (priv->dir);
  g_free (priv->display);

  g_object_unref (priv->gsettings);

  g_free (priv->description);
  g_free (priv->message);

  g_free (priv->user);
  g_free (priv->command);

  g_free (priv);
  g_free (context);
}

/**
 * gksu_context_ref
 * @context: A #GksuContext struct
 *
 * Increments the reference count of the given #GksuContext.
 */
GksuContext*
gksu_context_ref (GksuContext *context)
{
  context->priv->ref_count++;
  return context;
}

/**
 * gksu_context_unref
 * @context: A #GksuContext struct
 *
 * Decrements the reference count of the given #GksuContext struct,
 * freeing it if the reference count falls to zero.
 */
void
gksu_context_unref (GksuContext *context)
{
  if (--context->priv->ref_count == 0)
    gksu_context_free (context);
}

GType
gksu_context_get_type (void)
{
  static GType type_gksu_context = 0;

  if (!type_gksu_context)
    type_gksu_context = g_boxed_type_register_static
      ("GksuContext", 
       (GBoxedCopyFunc) gksu_context_ref,
       (GBoxedFreeFunc) gksu_context_unref);

  return type_gksu_context;
}

/**
 * gksu_su_full:
 * @context: a #GksuContext
 * @ask_pass: (scope call): a #GksuAskPassFunc to be called when
 * the library determines requesting a password is necessary; it
 * may be NULL, in which case the standard password request
 * dialogue will be used
 * @ask_pass_data: a #gpointer with user data to be passed to the
 * #GksuAskPasswordFunc
 * @pass_not_needed: (scope call) (nullable): a
 * #GksuPassNotNeededFunc that will be called when the command is
 * being run without the need for requesting a password; it will
 * only be called if the display-no-pass-info gsettings key is
 * enabled; NULL will have the standard dialogue be shown
 * @pass_not_needed_data: a #gpointer with the user data to be
 * passed to the #GksuPasswordNotNeededFunc
 * @exit_status: (nullable): an optional pointer to a #gint8 which
 * will be filled with the exit status of the child process
 * @error: a #GError object to be filled with the error code or NULL
 *
 * This could be considered one of the main functions in gksu.
 * It is responsible for doing the "user changing" magic calling
 * the #GksuAskPassFunc function to request a password if needed
 * and the #GksuPassNotNeededFunc function if a password won't be
 * needed, so the application has the opportunity of warning the
 * user what it's doing.
 *
 * This function uses su as a back end.
 *
 * Returns: TRUE if all went fine, FALSE if failed
 */
gboolean
gksu_su_full (GksuContext *context,
              GksuAskPassFunc ask_pass,
              gpointer ask_pass_data,
              GksuPassNotNeededFunc pass_not_needed,
              gpointer pass_not_needed_data,
              gint8 *exit_status,
              GError **error)
{
  GksuContextPrivate *priv = context->priv;
  GQuark gksu_quark;
  int i = 0;

  char auxcommand[] = LIBEXECDIR "/gksu/gksu-run-helper";

  int fdpty;
  pid_t pid;

  priv->sudo_mode = FALSE;

  gksu_quark = g_quark_from_string (PACKAGE);

  if (priv->command == NULL)
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_NOCOMMAND,
		   _("gksu_run needs a command to be run, "
		     "none was provided."));
      return FALSE;
    }

  if (!g_file_test (auxcommand, G_FILE_TEST_IS_EXECUTABLE))
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_HELPER,
		   _("The gksu-run-helper command was not found or "
		     "is not executable."));
      return FALSE;
    }

  if (!prepare_xauth (context))
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_XAUTH,
		   _("Unable to copy the user's Xauthorization file."));
      return FALSE;
    }

  pid = forkpty(&fdpty, NULL, NULL, NULL);
  if (pid == 0)
    {
      char **cmd = g_malloc0 (sizeof (char *) * 7);
      char *cmd_bin;

      /* Make us the session leader. */
      setsid ();
      cmd[i] = g_strdup ("su"); i++;
      if (priv->login_shell)
	{
	  cmd[i] = g_strdup ("-"); i++;
	}
      cmd[i] = g_strdup (priv->user); i++;
      if (priv->keep_env)
	{
	  cmd[i] = g_strdup ("-p"); i++;
	}
      cmd[i] = g_strdup ("-c"); i++;

      /* needs to get X authorization prior to running the program */
      cmd[i] = g_strdup_printf ("%s \"%s\"", auxcommand,
				priv->command); i++;

      cmd[i] = NULL;

      cmd_bin = g_find_program_in_path (cmd[0]);
      if (cmd_bin != NULL)
        {
          /* executes the command */
          if (execv (cmd_bin, cmd) == -1)
            {
              g_fprintf (stderr,
                         "Unable to run /bin/su: %s",
                         strerror (errno));
            }
          g_free (cmd_bin);
        }
      g_strfreev (cmd);

      _exit(1);
    }
  else if (pid == -1)
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_FORK,
		   _("Failed to fork new process: %s"),
		   strerror(errno));
      return FALSE;
    }
  else
    {
      fd_set rfds;

      struct timeval tv;

      struct passwd *pwd = NULL;
      gint target_uid = -1;
      gint my_uid = 0;

      char buf[256] = {0};
      gint status;

      char *password = NULL;
      char *cmdline = NULL;
      gboolean password_needed = FALSE;
#ifdef ENABLE_KEYRING
      gboolean used_keyring = FALSE;
#endif

      my_uid = getuid();
      pwd = getpwnam (priv->user);
      if (pwd)
	target_uid = pwd->pw_uid;

      if (ask_pass == NULL)
	{
	  ask_pass = su_ask_password;
	}

      if (pass_not_needed == NULL)
	{
	  pass_not_needed = no_pass;
	}

      /* no need to ask for password if we're already root */
      if (my_uid != target_uid && my_uid)
	{
	  int count;
	  struct termios tio;

	  read (fdpty, buf, 255);
          if (priv->debug)
            g_fprintf (stderr, "gksu_context_run: buf: -%s-\n", buf);

	  /* make sure we notice that ECHO is turned off, if it gets
	     turned off */
	  tcgetattr (fdpty, &tio);
	  for (count = 0; (tio.c_lflag & ECHO) && count < 15; count++)
	    {
	      g_usleep (1000);
	      tcgetattr (fdpty, &tio);
	    }

	  if (!(tio.c_lflag & ECHO))
	    {
	      gboolean prompt_grab;
	      prompt_grab = g_settings_get_boolean (priv->gsettings, "prompt");

	      if (prompt_grab)
		gksu_prompt_grab (context);

#ifdef ENABLE_KEYRING
              /* Try to get the password from libsecret first, but only if we
	         have not been requested to always ask for the password. */
	      if (!priv->always_ask_password)
	        password = get_libsecret_password (context);
	      if (password == NULL)
		{
		  password = ask_pass (context, buf, ask_pass_data, error);
		  if (priv->debug)
		    {
                      g_fprintf (stderr, "no password on keyring\n");
		      if (password == NULL)
                        g_fprintf (stderr, "no password from ask_pass!\n");
		    }
		}
	      else
		{
                  if (priv->debug)
                    g_fprintf (stderr, "password from keyring found\n");
		  used_keyring = TRUE;
		}
#else
              password = ask_pass (context, buf, ask_pass_data, error);
              if (priv->debug && password == NULL)
                g_fprintf (stderr, "no password from ask_pass!\n");
#endif
	      if (password == NULL || (error && (*error)))
		{
                  if (priv->debug)
                    g_fprintf (stderr, "gksu_su_full: problem getting password - getting out\n");
                  if (priv->debug && error)
                    g_fprintf (stderr, "error: %s\n", (*error)->message);
		  nullify_password (&password);
		  return TRUE;
		}

	      write (fdpty, password, strlen(password) + 1);
	      write (fdpty, "\n", 1);
	      password_needed = TRUE;
	    }
	}

      if (priv->debug)
        g_fprintf (stderr, "DEBUG (run:after-pass) buf: -%s-\n", buf);
      if (strncmp (buf, "gksu", 4) != 0 && strncmp (buf, "su", 2) != 0)
	{
	  /* drop the \n echoed on password entry if su did request
	     a password */
	  if (password_needed)
	    read_line (fdpty, buf, 255);
          if (priv->debug)
            g_fprintf (stderr, "DEBUG (run:post-after-pass) buf: -%s-\n", buf);
	  read_line (fdpty, buf, 255);
          if (priv->debug)
            g_fprintf (stderr, "DEBUG (run:post-after-pass) buf: -%s-\n", buf);
	}

      FD_ZERO (&rfds);
      FD_SET (fdpty, &rfds);
      tv.tv_sec = 1;
      tv.tv_usec = 0;
      int loop_count = 0;
      while (TRUE)
	{
	  int retval = 0;

	  /* Red Hat's su shows the full path to su in its error messages. */
          if (strncmp (buf, "su:", 3) == 0 ||
              strncmp (buf, "/bin/su:", 7) == 0)
            {
              char **strings;

	      if (password != NULL)
		{
		  nullify_password (&password);
#ifdef ENABLE_KEYRING
		  unset_libsecret_password (context);
#endif
		}

	      strings = g_strsplit (buf, ":", 2);

	      /* Red Hat and Fedora use 'incorrect password'. */
	      if (strings[1] &&
	          (g_str_has_prefix(strings[1], " Authentication failure") ||
	           g_str_has_prefix(strings[1], " incorrect password")))
		{
#ifdef ENABLE_KEYRING
		  if (used_keyring)
		    g_set_error (error, gksu_quark,
				 GKSU_ERROR_WRONGAUTOPASS,
				 _("Wrong password got from keyring."));
		  else
		    g_set_error (error, gksu_quark,
				 GKSU_ERROR_WRONGPASS,
				 _("Wrong password."));
#else
                  g_set_error (error, gksu_quark,
                               GKSU_ERROR_WRONGPASS,
                               _("Wrong password."));
#endif
		}
	      g_strfreev (strings);

              if (priv->debug)
                g_fprintf (stderr, "DEBUG (auth_failed) buf: -%s-\n", buf);

	      break;
	    }
          else if (strncmp (buf, "gksu: waiting", 13) == 0)
	    {
              char *line;

	      if (password != NULL)
		{
#ifdef ENABLE_KEYRING
		  set_libsecret_password (context, password);
#endif
		  nullify_password (&password);
		}

              if (priv->debug)
                g_fprintf (stderr, "DEBUG (gksu: waiting) buf: -%s-\n", buf);

	      line = g_strdup_printf ("gksu-run: %s\n", priv->display);
	      write (fdpty, line, strlen(line));
	      g_free (line);

	      line = g_strdup_printf ("gksu-run: %s\n", priv->xauth);
	      write (fdpty, line, strlen(line));
	      g_free (line);

#ifndef __FreeBSD_kernel__
	      tcdrain (fdpty);
#endif

              memset (buf, '\0', 256);
	      read (fdpty, buf, 255);

	      break;
	    }

	  retval = select (fdpty + 1, &rfds, NULL, NULL, &tv);
	  if ((loop_count > 50) || (!retval))
            {
              char *emsg = NULL;
              char *converted_str = NULL;
              GError *converr = NULL;

	      if (password)
		nullify_password (&password);

	      converted_str = g_locale_to_utf8 (buf, -1, NULL, NULL, &converr);
	      if (converr)
		{
		  g_warning (_("Failed to communicate with "
			       "gksu-run-helper.\n\n"
			       "Received:\n"
			       " %s\n"
			       "While expecting:\n"
			       " %s"), buf, "gksu: waiting");
		  emsg = g_strdup_printf (_("Failed to communicate with "
					    "gksu-run-helper.\n\n"
					    "Received bad string "
					    "while expecting:\n"
					    " %s"), "gksu: waiting");
		  g_error_free (converr);
		}
	      else
		emsg = g_strdup_printf (_("Failed to communicate with "
					  "gksu-run-helper.\n\n"
					  "Received:\n"
					  " %s\n"
					  "While expecting:\n"
					  " %s"), converted_str, "gksu: waiting");
	      g_free (converted_str);

	      g_set_error_literal (error, gksu_quark, GKSU_ERROR_HELPER, emsg);
	      g_free (emsg);

	      if (priv->debug)
                g_fprintf (stderr, "DEBUG (failed!) buf: -%s-\n", buf);

	      return FALSE;
	    }
	  else if (retval == -1)
            {
              if (priv->debug)
                g_fprintf (stderr, "DEBUG (select failed!) buf: %s\n", buf);
	      return FALSE;
	    }

	  read (fdpty, buf, 255);
          if (priv->debug)
            g_fprintf (stderr, "DEBUG (run:after-pass) buf: -%s-\n", buf);
	  loop_count++;
	}

#ifdef ENABLE_KEYRING
      if (!password_needed || used_keyring)
#else
      if (!password_needed)
#endif
	{
	  gboolean should_display;

	  should_display = g_settings_get_boolean (priv->gsettings,
                                                   "display-no-pass-info");

	  /* configuration tells us to show this message */
	  if (should_display)
	    {
              if (priv->debug)
                g_fprintf (stderr, "Calling pass_not_needed window...\n");
	      pass_not_needed (context, pass_not_needed_data);
	      /* make sure it is displayed */
	      while (gtk_events_pending ())
		gtk_main_iteration ();
	    }
	}

      cmdline = g_strdup("bin/su");
      /* wait for the child process to end or become something other
	 than su */
      pid_t pid_exited;
      while ((!(pid_exited = waitpid (pid, &status, WNOHANG))) &&
	     (g_str_has_suffix(cmdline, "bin/su")))
	{
	  if (cmdline)
	    g_free (cmdline);
	  cmdline = get_process_name (pid);
          g_usleep (G_USEC_PER_SEC);
	}

      memset (buf, '\0', 256);
      while (read (fdpty, buf, 255) > 0)
	{
          g_fprintf (stderr, "%s", buf);
          memset (buf, '\0', 256);
	}

      if (pid_exited != pid)
	waitpid(pid, &status, 0);

      if (exit_status)
      {
      	if (WIFEXITED(status)) {
      	  *exit_status = WEXITSTATUS(status);
	} else if (WIFSIGNALED(status)) {
	  *exit_status = -1;
	}
      }

      if (WEXITSTATUS(status))
	{
	  if(cmdline)
	    {
	      /* su already exec()ed something else, don't report
	       * exit status errors in that case
	       */
	      if (!g_str_has_suffix (cmdline, "su"))
		{
		  g_free (cmdline);
		  return FALSE;
		}
	      g_free (cmdline);
	    }

	  if (error == NULL)
	    g_set_error (error, gksu_quark,
			 GKSU_ERROR_CHILDFAILED,
			 _("su terminated with %d status"),
			 WEXITSTATUS(status));
	}
    }

  if (error)
    return FALSE;

  return TRUE;
}

/**
 * gksu_su
 * @command_line: (not nullable): the command line that will be
 * executed as other user
 * @error: a #GError to be set with the error condition, if an
 * error happens
 *
 * This function is a wrapper for gksu_su_run_full(). It will call
 * it without giving the callback functions, which leads to the
 * standard ones being called. A simple #GksuContext is created to
 * hold the user name and the command.
 *
 * Returns: TRUE if all went well, FALSE if an error happend
 */
gboolean
gksu_su (const char *command_line, GError **error)
{
  GksuContext *context;
  gboolean retval;

  g_return_val_if_fail (command_line != NULL, FALSE);

  context = gksu_context_new ();
  gksu_context_set_command (context, command_line);
  gksu_context_set_user (context, "root");
  retval = gksu_su_full (context,
			 NULL, NULL,
			 NULL, NULL,
			 NULL, error);
  gksu_context_free (context);
  return retval;
}

/**
 * gksu_sudo_full:
 * @context: a #GksuContext
 * @ask_pass: (scope call): a #GksuAskPassFunc to be called when
 * the library determines requesting a password is necessary; it
 * may be NULL, in which case the standard password request
 * dialogue will be used
 * @ask_pass_data: a #gpointer with user data to be passed to the
 * #GksuAskPasswordFunc
 * @pass_not_needed: (scope call) (nullable): a
 * #GksuPassNotNeededFunc that will be called when the command is
 * being run without the need for requesting a password; it will
 * only be called if the display-no-pass-info gsettings key is
 * enabled; NULL will have the standard dialogue be shown
 * @pass_not_needed_data: a #gpointer with the user data to be
 * passed to the #GksuPasswordNotNeededFunc
 * @error: a #GError object to be filled with the error code or NULL
 * @exit_status: (nullable): an optional pointer to a #gint8 which
 * will be filled with the exit status of the child process
 *
 * This could be considered one of the main functions in gksu.
 * It is responsible for doing the "user changing" magic calling
 * the #GksuAskPassFunc function to request a password if needed
 * and the #GksuPassNotNeededFunc function if a password won't be
 * needed, so the application has the opportunity of warning the
 * user what it's doing.
 *
 * This function uses a sudo back end.
 *
 * Returns: TRUE if all went fine, FALSE if failed
 */
gboolean
gksu_sudo_full (GksuContext *context,
                GksuAskPassFunc ask_pass,
                gpointer ask_pass_data,
                GksuPassNotNeededFunc pass_not_needed,
                gpointer pass_not_needed_data,
                gint8 *exit_status,
                GError **error)
{
  GksuContextPrivate *priv = context->priv;
  char **cmd;
  char buffer[256] = {0};
  char *child_stderr = NULL;

#ifdef SUDO_FORKPTY
  /* This command is used to gain a token */
  char *const verifycmd[] =
    {
      "sudo", "-p", "GNOME_SUDO_PASS", "-v", NULL
    };
#endif

  int argcount = 8;
  int i, j;

  GQuark gksu_quark;

  char **exec_env = NULL;
  char *xauth = NULL;

  pid_t pid;
  int status;
#ifdef SUDO_FORKPTY
  FILE *fdfile = NULL;
  int fdpty = -1;
#else
  FILE *infile, *outfile;
  int parent_pipe[2];  /* For talking to the parent */
  int child_pipe[2];   /* For talking to the child */
#endif

  priv->sudo_mode = TRUE;

  gksu_quark = g_quark_from_string (PACKAGE);

  if (priv->command == NULL)
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_NOCOMMAND,
		   _("gksu_sudo_run needs a command to be run, "
		     "none was provided."));
      return FALSE;
    }

  if (ask_pass == NULL)
    {
      if (priv->debug)
        g_fprintf (stderr, "No ask_pass set, using default!\n");
      ask_pass = su_ask_password;
    }
  if (pass_not_needed == NULL)
    {
      pass_not_needed = no_pass;
    }

  if (priv->always_ask_password)
    {
       int exit_status = 0;
       g_spawn_command_line_sync ("sudo -K", NULL, NULL, &exit_status, NULL);
    }


  /*
     FIXME: need to set GError in a more detailed way
  */
  if (!sudo_prepare_xauth (context))
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_XAUTH,
		   _("Unable to copy the user's Xauthorization file."));
      return FALSE;
    }

  exec_env = g_get_environ ();

  /* Cleanup the environment; some variables we bring from the user
   * environment make dconf-based applications misbehaving these days.
   */
  exec_env = g_environ_unsetenv (exec_env, "DBUS_SESSION_BUS_ADDRESS");
  exec_env = g_environ_unsetenv (exec_env, "XDG_RUNTIME_DIR");
  exec_env = g_environ_unsetenv (exec_env, "ORBIT_SOCKETDIR");

  /* sets XAUTHORITY */
  xauth = g_strdup_printf ("%s/.Xauthority", priv->dir);
  exec_env = g_environ_setenv (exec_env, "XAUTHORITY", xauth, TRUE);
  if (priv->debug)
    g_fprintf (stderr, "xauth: %s\n", xauth);
  g_free (xauth);

  cmd = g_new0 (char *, argcount + 1);

  argcount = 0;

  /* sudo binary */
  cmd[argcount] = g_strdup("sudo");
  argcount++;

  if (!priv->keep_env)
    {
      /* Make sudo set $HOME */
      cmd[argcount] = g_strdup("-H");
      argcount++;
    }
  else
    {
      /* Preserve the environment, if sudo will let us */
      cmd[argcount] = g_strdup("-E");
      argcount++;
    }

  /* Make sudo read from stdin */
  cmd[argcount] = g_strdup("-S");
  argcount++;

#ifdef SUDO_FORKPTY
  /* Make sudo noninteractive (we should already have a token) */
  cmd[argcount] = g_strdup("-n");
  argcount++;
#endif

  /* Make sudo use next arg as prompt */
  cmd[argcount] = g_strdup("-p");
  argcount++;

  /* prompt */
  cmd[argcount] = g_strdup("GNOME_SUDO_PASS");
  argcount++;

  /* Make sudo use the selected user */
  cmd[argcount] = g_strdup("-u");
  argcount++;

  /* user */
  cmd[argcount] = g_strdup(priv->user);
  argcount++;

  /* sudo does not understand this if we do not use -H
     weird.
  */
  if (!priv->keep_env)
    {
      /* Make sudo stop processing options */
      cmd[argcount] = g_strdup("--");
      argcount++;
    }

  {
    char *tmp_arg = g_malloc0 (sizeof (char) * 1);
    gboolean inside_quotes = FALSE;

    tmp_arg[0] = '\0';

    for (i = j = 0; ; i++)
      {
	if ((priv->command[i] == '\'') && (priv->command[i-1] != '\\'))
	  {
	    i = i + 1;
	    inside_quotes = !inside_quotes;
	  }

	if ((priv->command[i] == ' ' && inside_quotes == FALSE) ||
	    priv->command[i] == '\0')
	  {
            tmp_arg = g_realloc (tmp_arg, sizeof (char) * (j + 1));
            tmp_arg[j] = '\0';
            cmd = g_realloc (cmd, sizeof (char *) * (argcount + 1));
            cmd[argcount] = g_strdup (tmp_arg);

	    g_free (tmp_arg);

	    argcount = argcount + 1;
	    j = 0;

	    if (priv->command[i] == '\0')
	      break;

            tmp_arg = g_malloc (sizeof (char) * 1);
	    tmp_arg[0] = '\0';
	  }
	else
	  {
	    if (priv->command[i] == '\\' && priv->command[i+1] != '\\')
	      i = i + 1;
            tmp_arg = g_realloc (tmp_arg, sizeof (char) * (j + 1));
	    tmp_arg[j] = priv->command[i];
	    j = j + 1;
	  }
      }
  }
  cmd = g_realloc (cmd, sizeof (char *) * (argcount + 1));
  cmd[argcount] = NULL;

  if (priv->debug)
    {
      for (i = 0; cmd[i] != NULL; i++)
        g_fprintf (stderr, "cmd[%d]: %s\n", i, cmd[i]);
    }

#ifdef SUDO_FORKPTY
  pid = forkpty(&fdpty, NULL, NULL, NULL);
#else
  if ((pipe(parent_pipe)) == -1)
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_PIPE,
                   _("Error creating pipe: %s"),
                   strerror(errno));
      g_strfreev (exec_env);
      sudo_reset_xauth (context);
      return FALSE;
    }
  if ((pipe(child_pipe)) == -1)
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_PIPE,
                   _("Error creating pipe: %s"),
                   strerror(errno));
      g_strfreev (exec_env);
      sudo_reset_xauth (context);
      return FALSE;
    }

  pid = fork();
#endif

  if (pid == 0)
    {
      /* Child. */
      char *cmd_bin;

       /* Make us session leader. */
      setsid ();

#ifdef SUDO_FORKPTY
      cmd_bin = g_find_program_in_path (verifycmd[0]);
      execve (cmd_bin, verifycmd, exec_env);
#else
      close (child_pipe[1]);
      dup2 (child_pipe[0], STDIN_FILENO);
      dup2 (parent_pipe[1], STDERR_FILENO);
      cmd_bin = g_find_program_in_path (cmd[0]);
      execve (cmd_bin, cmd, exec_env);
#endif
      g_free (cmd_bin);

      g_set_error (error, gksu_quark, GKSU_ERROR_EXEC,
		   _("Failed to exec new process: %s"),
		   strerror(errno));
      g_strfreev (exec_env);
      sudo_reset_xauth (context);
      return FALSE;
    }
  else if (pid == -1)
    {
      g_set_error (error, gksu_quark, GKSU_ERROR_FORK,
		   _("Failed to fork new process: %s"),
		   strerror(errno));
      g_strfreev (exec_env);
      sudo_reset_xauth (context);
      return FALSE;
    }

  else
    {
      int counter = 0;
      char *cmdline = NULL;

      /* Parent. */
#ifdef SUDO_FORKPTY
      fdfile = fdopen(fdpty, "w+");

      fcntl (fdpty, F_SETFL, O_NONBLOCK);
#else
      close (parent_pipe[1]);

      infile = fdopen(parent_pipe[0], "r");
      if (!infile)
       {
         g_set_error (error, gksu_quark, GKSU_ERROR_PIPE,
                      _("Error opening pipe: %s"),
                      strerror(errno));
         g_strfreev (exec_env);
         sudo_reset_xauth (context);
         return FALSE;
       }

      outfile = fdopen(child_pipe[1], "w");
      if (!outfile)
       {
         g_set_error (error, gksu_quark, GKSU_ERROR_PIPE,
                      _("Error opening pipe: %s"),
                      strerror(errno));
         g_strfreev (exec_env);
         sudo_reset_xauth (context);
         return FALSE;
       }

      fcntl (parent_pipe[0], F_SETFL, O_NONBLOCK);
#endif

      { /* no matter if we can read, since we're using
	   O_NONBLOCK; this is just to avoid the prompt
	   showing up after the read */
	fd_set rfds;
	struct timeval tv;

	FD_ZERO(&rfds);
#ifdef SUDO_FORKPTY
	FD_SET(fdpty, &rfds);
#else
        FD_SET(parent_pipe[0], &rfds);
#endif
	tv.tv_sec = 1;
	tv.tv_usec = 0;

#ifdef SUDO_FORKPTY
	select (fdpty + 1, &rfds, NULL, NULL, &tv);
#else
        select (parent_pipe[0] + 1, &rfds, NULL, NULL, &tv);
#endif
      }

      /* Try hard to find the prompt; it may happen that we're
       * seeing sudo's lecture, or that some pam module is spitting
       * debugging stuff at the screen
       */
      for (counter = 0; counter < 50; counter++)
	{
	  if (strncmp (buffer, "GNOME_SUDO_PASS", 15) == 0)
	    break;

#ifdef SUDO_FORKPTY
	  read_line (fdpty, buffer, 256);
#else
          read_line (parent_pipe[0], buffer, 256);
#endif

          if (priv->debug)
            g_fprintf (stderr, "buffer: -%s-\n", buffer);

          g_usleep(1000);
        }

      if (priv->debug)
        g_fprintf (stderr, "brute force GNOME_SUDO_PASS ended...\n");

      if (strncmp(buffer, "GNOME_SUDO_PASS", 15) == 0)
        {
          char *password = NULL;
          gboolean prompt_grab;

          if (priv->debug)
            g_fprintf (stderr, "Yeah, we're in...\n");

	  prompt_grab = g_settings_get_boolean (priv->gsettings, "prompt");
	  if (prompt_grab)
	    gksu_prompt_grab (context);

	  password = ask_pass (context, _("Password: "),
			       ask_pass_data, error);
	  if (password == NULL || (*error))
	    {
	      nullify_password (&password);
	      return FALSE;
	    }

          g_usleep (1000);

#ifdef SUDO_FORKPTY
	  write (fdpty, password, strlen(password) + 1);
	  write (fdpty, "\n", 1);
#else
          g_fprintf (outfile, "%s\n", password);
          fclose (outfile);
#endif

          nullify_password (&password);

#ifdef SUDO_FORKPTY
	  fcntl(fdpty, F_SETFL, fcntl(fdpty, F_GETFL) & ~O_NONBLOCK);

	  /* ignore the first newline that comes right after sudo receives
	     the password */
	  fgets (buffer, 255, fdfile);
          if (g_strcmp0 (buffer, "\n") == 0)
            {
              /* this is the status we are interested in */
              fgets (buffer, 255, fdfile);
            }
#else
          fcntl(parent_pipe[0], F_SETFL, fcntl(parent_pipe[0], F_GETFL) & ~O_NONBLOCK);

	  /* ignore the first newline that comes right after sudo receives
	     the password */
	  fgets (buffer, 255, infile);
          if (g_strcmp0 (buffer, "\n") == 0)
            {
              /* this is the status we are interested in */
              fgets (buffer, 255, infile);
            }
#endif
	}
      else
	{
	  gboolean should_display;
          if (priv->debug)
            g_fprintf (stderr, "No password prompt found; "
                               "we'll assume we don't need a password.\n");

          /* turn NONBLOCK off, also if have no prompt */
#ifdef SUDO_FORKPTY
          fcntl(fdpty, F_SETFL, fcntl(fdpty, F_GETFL) & ~O_NONBLOCK);
#else
          fcntl(parent_pipe[0], F_SETFL, fcntl(parent_pipe[0], F_GETFL) & ~O_NONBLOCK);
#endif

	  should_display = g_settings_get_boolean (priv->gsettings,
						  "display-no-pass-info");

	  /* configuration tells us to show this message */
	  if (should_display)
	    {
              if (priv->debug)
                g_fprintf (stderr, "Calling pass_not_needed window...\n");
	      pass_not_needed (context, pass_not_needed_data);
	      /* make sure it is displayed */
	      while (gtk_events_pending ())
		gtk_main_iteration ();
	    }

          g_fprintf (stderr, "%s", buffer);
	}

      if (g_str_has_prefix (buffer, "Sorry, try again.") ||
          g_str_has_prefix (buffer, "GNOME_SUDO_PASSSorry, try again.") ||
          g_str_has_prefix (buffer, g_dgettext ("sudoers", "Sorry, try again.")))
        {
          g_set_error (error, gksu_quark, GKSU_ERROR_WRONGPASS,
                       _("Wrong password."));
        }
      else
	{
          char *haystack = buffer;
          char *needle;

	  needle = g_strstr_len (haystack, strlen (haystack), " ");
	  if (needle != NULL && strlen (needle) >= 2)
	    {
	      needle = &needle[1];
	      if (strncmp (needle, "is not in", 9) == 0)
		g_set_error (error, gksu_quark, GKSU_ERROR_NOT_ALLOWED,
			     _("The underlying authorization mechanism (sudo) "
			       "does not allow you to run this program. Contact "
			       "the system administrator."));
	    }
	}

      /* If we have an error, let's just stop sudo right there. */
#ifdef SUDO_FORKPTY
      if (error)
        close(fdpty);
#else
      if (error)
        fclose(infile);
#endif

      cmdline = g_strdup("sudo");
      /* wait for the child process to end or become something other
	 than sudo */
      pid_t pid_exited;
      while ((!(pid_exited = waitpid (pid, &status, WNOHANG))) &&
	     (g_str_has_suffix(cmdline, "sudo")))
	{
	  if (cmdline)
	    g_free (cmdline);
	  cmdline = get_process_name (pid);
          g_usleep (G_USEC_PER_SEC);
	}

      /* if the process is still active waitpid() on it */
      if (pid_exited != pid)
	waitpid(pid, &status, 0);

      g_strfreev (exec_env);
      sudo_reset_xauth (context);

#if SUDO_FORKPTY
      /*
       * Did token acquisition succeed? If so, spawn sudo in
       * non-interactive mode. It should either succeed or die
       * immediately if you're not allowed to run the command.
       */
      if (WEXITSTATUS(status) == 0)
        {
          g_spawn_sync(NULL, cmd, NULL, 0, NULL, NULL,
                       NULL, &child_stderr, &status,
                       error);
        }
#endif

      if (exit_status)
      {
      	if (WIFEXITED(status)) {
      	  *exit_status = WEXITSTATUS(status);
	} else if (WIFSIGNALED(status)) {
	  *exit_status = -1;
	}
      }

      if (WEXITSTATUS(status))
	{
          if (child_stderr != NULL &&
              g_str_has_prefix (child_stderr, "Sorry, user "))
            {
              g_set_error (error, gksu_quark, GKSU_ERROR_NOT_ALLOWED,
                           _("The underlying authorization mechanism (sudo) "
                             "does not allow you to run this program. Contact "
                             "the system administrator."));
            }
	  if(cmdline)
	    {
	      /* sudo already exec()ed something else, don't report
	       * exit status errors in that case
	       */
	      if (!g_str_has_suffix (cmdline, "sudo"))
		{
		  g_free (cmdline);
		  g_free (child_stderr);
		  return FALSE;
		}
	      g_free (cmdline);
	    }
	  if (error == NULL)
	    g_set_error (error, gksu_quark,
			 GKSU_ERROR_CHILDFAILED,
			 _("sudo terminated with %d status"),
			 WEXITSTATUS(status));
	}
    }

  if (child_stderr != NULL)
    {
      g_fprintf (stderr, "%s", child_stderr);
      g_free (child_stderr);
    }

  /* if error is set we have found an error condition */
  return (error == NULL);
}

/**
 * gksu_sudo
 * @command_line: (not nullable): the command line that will be
 * executed as other user
 * @error: a #GError to be set with the error condition, if an
 * error happens
 *
 * This function is a wrapper for gksu_sudo_run_full(). It will
 * call it without giving the callback functions, which leads to
 * the standard ones being called. A simple #GksuContext is created
 * to hold the user name and the command.
 *
 * Returns: TRUE if all went well, FALSE if an error happend
 */
gboolean
gksu_sudo (const char *command_line,
	   GError **error)
{
  GksuContext *context;
  gboolean retval;

  g_return_val_if_fail (command_line != NULL, FALSE);

  context = gksu_context_new ();
  gksu_context_set_command (context, command_line);
  gksu_context_set_user (context, "root");
  retval = gksu_sudo_full (context,
			   NULL, NULL,
			   NULL, NULL,
			   NULL, error);
  gksu_context_free (context);

  return retval;
}

/**
 * gksu_run_full:
 * @context: a #GksuContext
 * @ask_pass: (scope call): a #GksuAskPassFunc to be called when
 * the library determines requesting a password is necessary; it
 * may be NULL, in which case the standard password request
 * dialogue will be used
 * @ask_pass_data: a #gpointer with user data to be passed to the
 * #GksuAskPasswordFunc
 * @pass_not_needed: (scope call) (nullable): a
 * #GksuPassNotNeededFunc that will be called
 * when the command is being run without the need for requesting a
 * password; it will only be called if the display-no-pass-info
 * gsettings key is enabled; NULL will have the standard dialogue
 * be shown
 * @pass_not_needed_data: a #gpointer with the user data to be
 * passed to the #GksuPasswordNotNeededFunc
 * @exit_status: (nullable): an optional pointer to a #gint8 which
 * will be filled with the exit status of the child process
 * @error: a #GError object to be filled with the error code or NULL
 *
 * This function is a wrapper for gksu_sudo_full()/gksu_su_full().
 * It will call one of them, depending on the GSettings key that
 * defines whether the default behaviour for gksu is su or sudo
 * mode. This is the recommended way of using the library
 * functionality.
 *
 * Returns: TRUE if all went fine, FALSE if failed
 */
gboolean
gksu_run_full (GksuContext *context,
               GksuAskPassFunc ask_pass,
               gpointer ask_pass_data,
               GksuPassNotNeededFunc pass_not_needed,
               gpointer pass_not_needed_data,
               gint8 *exit_status,
               GError **error)
{
  GSettings *gsettings;
  gboolean sudo_mode;

  gsettings = g_settings_new (GSETTINGS_SCHEMA);
  sudo_mode = g_settings_get_boolean (gsettings, "sudo-mode");
  g_object_unref (gsettings);

  if (sudo_mode)
    return gksu_sudo_full (context, ask_pass, ask_pass_data,
                           pass_not_needed, pass_not_needed_data,
                           exit_status, error);

  return gksu_su_full (context, ask_pass, ask_pass_data,
                       pass_not_needed, pass_not_needed_data,
                       exit_status, error);
}

/**
 * gksu_run
 * @command_line: (not nullable): the command line that will be
 * executed as other user
 * @error: a #GError to be set with the error condition, if an
 * error happens
 *
 * This function is a wrapper for gksu_sudo()/gksu_su(). It will
 * call one of them, depending on the GSettings key that defines
 * whether the default behaviour for gksu is su or sudo mode. This
 * is the recommended way of using the library functionality.
 *
 * Returns: FALSE if all went well, TRUE if an error happend
 */
gboolean
gksu_run (const char *command_line,
	  GError **error)
{
  GSettings *gsettings;
  gboolean sudo_mode;

  g_return_val_if_fail (command_line != NULL, FALSE);

  gsettings = g_settings_new (GSETTINGS_SCHEMA);
  sudo_mode = g_settings_get_boolean (gsettings, "sudo-mode");
  g_object_unref (gsettings);

  if (sudo_mode)
    return gksu_sudo (command_line, error);

  return gksu_su (command_line, error);
}

/**
 * gksu_ask_password_full:
 * @context: a #GksuContext
 * @prompt: (nullable): a prompt different from "Password: "
 * @error: a #GError object to be filled with the error code or NULL
 *
 * This function uses the gksu infrastructure to request for a
 * password, but instead of passing it to su or sudo to run a
 * command it simply returns the password.
 *
 * Returns: (transfer full): a newly allocated string with the
 * password
 */
char *
gksu_ask_password_full (GksuContext *context, const char *prompt,
			GError **error)
{
  return su_ask_password (context, prompt, NULL, error);
}

/**
 * gksu_ask_password
 * @error: a #GError to be set with the error condition, if an
 * error happens
 *
 * This function uses the gksu infrastructure to request for a
 * password, but instead of passing it to su or sudo to run a
 * command it simply returns the password. This is just a
 * convenience wrapper for gksu_ask_password_full().
 *
 * Returns: (transfer full): a newly allocated string with the
 * password
 */
char *
gksu_ask_password (GError **error)
{
  GksuContext *context = gksu_context_new ();
  char *retval;

  gksu_context_set_user (context, "root");
  retval = gksu_ask_password_full (context, NULL, error);
  gksu_context_free (context);

  return retval;
}
