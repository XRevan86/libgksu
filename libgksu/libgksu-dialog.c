/*
 * libgksu-dialog -- GTK+ widget and convenience functions
 * Copyright (C) 2004-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <https://gnu.org/licenses>.
 */

#include <string.h>

#include "defines.h"

#include "libgksu-dialog.h"

struct _GksuDialogPrivate {
  GtkWidget *main_vbox;
  GtkWidget *hbox;
  GtkWidget *image;
  GtkWidget *entry;
  GtkWidget *entry_vbox;
  GtkWidget *alert;
  GtkWidget *label;
  GtkWidget *label_warn_capslock;
  GtkWidget *ok_button;
  GtkWidget *cancel_button;
  GtkWidget *prompt_label;
  gboolean grab;
  gboolean sudo_mode;
};

enum {
  GKSU_DIALOG_GRAB = 1,
  GKSU_DIALOG_SUDO_MODE = 2,
};

static void
gksu_dialog_class_init (GksuDialogClass *klass);

static void
gksu_dialog_init (GksuDialog *gksu_dialog);

#ifdef ENABLE_KEYRING
static void
gksu_dialog_create_keyring_ui (GksuDialog *gksu_dialog);
#endif

GType
gksu_dialog_get_type (void)
{
  static GType type = 0;

  if (type == 0)
    {
      static const GTypeInfo info =
	{
	  sizeof (GksuDialogClass), /* size of class */
	  NULL, /* base_init */
	  NULL, /* base_finalize */
	  (GClassInitFunc) gksu_dialog_class_init,
	  NULL, /* class_finalize */
	  NULL, /* class_data */
	  sizeof (GksuDialog), /* size of object */
	  0, /* n_preallocs */
	  (GInstanceInitFunc) gksu_dialog_init /* instance_init */
	};
      type = g_type_register_static (gtk_dialog_get_type (),
				     "GksuDialogType",
				     &info, 0);
    }

  return type;
}

static void
gksu_dialog_set_property (GObject *object, guint property_id,
                          const GValue *value, GParamSpec *spec)
{
  GksuDialog *self = (GksuDialog*) object;

  switch (property_id)
    {
    case GKSU_DIALOG_GRAB:
      self->priv->grab = g_value_get_boolean (value);
      break;
    case GKSU_DIALOG_SUDO_MODE:
      self->priv->sudo_mode = g_value_get_boolean (value);
#ifdef ENABLE_KEYRING
      if (!self->priv->sudo_mode)
	gksu_dialog_create_keyring_ui ((GksuDialog*) object);
#endif
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, spec);
      break;
    }
}

static void
gksu_dialog_get_property (GObject *object, unsigned int property_id,
                          GValue *value, GParamSpec *spec)
{
  GksuDialog *self = (GksuDialog*) object;

  switch (property_id)
    {
    case GKSU_DIALOG_GRAB:
      g_value_set_boolean (value, self->priv->grab);
      break;
    case GKSU_DIALOG_SUDO_MODE:
      g_value_set_boolean (value, self->priv->sudo_mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, spec);
      break;
    }
}

static void
gksu_dialog_class_init (GksuDialogClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GParamSpec *spec;

  gobject_class->set_property = gksu_dialog_set_property;
  gobject_class->get_property = gksu_dialog_get_property;

  spec = g_param_spec_boolean ("grab",
                               "Whether we are running in grab mode",
                               "Set/Get whether we are running in grab mode",
                               FALSE, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  g_object_class_install_property (gobject_class, GKSU_DIALOG_GRAB, spec);

  spec = g_param_spec_boolean ("sudo-mode",
			       "Whether we are running in sudo mode",
			       "Set/Get whether we are running in sudo mode",
			       FALSE, G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  g_object_class_install_property (gobject_class, GKSU_DIALOG_SUDO_MODE, spec);

  g_type_class_add_private (klass, sizeof (GksuDialogPrivate));
}

static gboolean
verify_capslock_cb (GdkKeymap *keymap, gpointer data)
{
  GksuDialog *dialog = (GksuDialog *) data;
  GtkWidget *label = dialog->priv->label_warn_capslock;
  gboolean is_capslock_on = gdk_keymap_get_caps_lock_state (keymap);

  if (GTK_IS_LABEL(label) && is_capslock_on)
    gtk_label_set_markup (GTK_LABEL(label), _("<b>You have capslock on</b>"));
  else if (GTK_IS_LABEL(label))
    gtk_label_set_text (GTK_LABEL(label), "");

  return FALSE;
}

void
set_sensitivity_cb (GtkWidget *button, gpointer data)
{
  GtkWidget *widget = (GtkWidget*)data;
  gboolean sensitive;

  sensitive = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(button));
  gtk_widget_set_sensitive (widget, sensitive);
}

#ifdef ENABLE_KEYRING
static void
cb_toggled_cb (GtkWidget *button, gpointer data)
{
  GSettings *gsettings;
  char *key;
  gboolean toggled;
  char *key_name;

  g_return_if_fail (data != NULL);

  key_name = (char *) data;

  gsettings = g_settings_new (GSETTINGS_SCHEMA);
  toggled = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(button));

  key = g_strdup_printf ("%s", key_name);

  if (g_strcmp0 (key_name, "save-keyring") == 0)
    {
      if (toggled)
	g_settings_set_string (gsettings, key, "session");
      else
	g_settings_set_string (gsettings, key, "default");
    }
  else
    g_settings_set_boolean (gsettings, key, toggled);

  g_object_unref (gsettings);

  g_free (key);
}

static void
cb_label_realize_cb (GtkWidget *widget,
                     gpointer   data)
{
  GtkWidget *radio_vbox = (GtkWidget *) data;
#if !GTK_CHECK_VERSION (3, 0, 0)
  GtkWidget *radio_vbox_fixed;
#endif
  gint radio_vbox_margin_start = 0;

#if !GTK_CHECK_VERSION (3, 0, 0)
  radio_vbox_fixed = gtk_widget_get_parent (radio_vbox);
#endif
  gtk_widget_translate_coordinates (widget, gtk_widget_get_parent (widget),
                                    0, 0, &radio_vbox_margin_start, NULL);

#if GTK_CHECK_VERSION (3, 12, 0)
  gtk_widget_set_margin_start (radio_vbox, radio_vbox_margin_start);
#elif GTK_CHECK_VERSION (3, 0, 0)
  gtk_widget_set_margin_left (radio_vbox, radio_vbox_margin_start);
#else
  gtk_fixed_move (GTK_FIXED(radio_vbox_fixed), radio_vbox,
                  radio_vbox_margin_start, 0);
#endif
}

static void
gksu_dialog_create_keyring_ui (GksuDialog *dialog)
{
  /* keyring stuff */
  GtkWidget *vbox;
  GtkWidget *check_button;
  GtkWidget *radio_vbox;
#if !GTK_CHECK_VERSION (3, 0, 0)
  GtkWidget *radio_vbox_fixed;
#endif
  GtkWidget *radio_session, *radio_default;

  GSettings *gsettings;
  gboolean remember_password;
  char *tmp = NULL;

  /* keyring stuff */
  gsettings = g_settings_new (GSETTINGS_SCHEMA);

#if GTK_CHECK_VERSION (3, 0, 0)
  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
  gtk_box_set_homogeneous (GTK_BOX(vbox), TRUE);
#else
  vbox = gtk_vbox_new (TRUE, 2);
#endif
  gtk_box_pack_start (GTK_BOX(GKSU_DIALOG(dialog)->priv->entry_vbox),
                      vbox, TRUE, TRUE, 0);
  gtk_widget_show (vbox);

  check_button = gtk_check_button_new_with_label (_("Remember password"));
  g_signal_connect (G_OBJECT(check_button), "toggled",
                    G_CALLBACK(cb_toggled_cb), "save-to-keyring");

  remember_password = g_settings_get_boolean (gsettings, "save-to-keyring");
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(check_button),
                                remember_password);
  gtk_box_pack_start (GTK_BOX(vbox), check_button, TRUE, FALSE, 0);
  gtk_widget_show (check_button);

#if GTK_CHECK_VERSION (3, 0, 0)
  radio_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 6);
  gtk_box_set_homogeneous (GTK_BOX(radio_vbox), TRUE);
  gtk_box_pack_start (GTK_BOX(vbox), radio_vbox, TRUE, TRUE, 0);
#else
  radio_vbox = gtk_vbox_new (TRUE, 6);
  radio_vbox_fixed = gtk_fixed_new ();
  gtk_container_add (GTK_CONTAINER(radio_vbox_fixed), radio_vbox);

  gtk_box_pack_start (GTK_BOX(vbox), radio_vbox_fixed, TRUE, TRUE, 0);
  gtk_widget_show (radio_vbox_fixed);
#endif
  gtk_widget_set_sensitive (radio_vbox, remember_password);
  gtk_widget_show (radio_vbox);

  g_signal_connect (G_OBJECT(gtk_bin_get_child (GTK_BIN(check_button))),
                    "realize", G_CALLBACK(cb_label_realize_cb),
                    (gpointer) radio_vbox);

  radio_session = gtk_radio_button_new_with_label (NULL,
                                                   _("Save for this session"));
  g_signal_connect (G_OBJECT(radio_session), "toggled",
                    G_CALLBACK(cb_toggled_cb), "save-keyring");
  gtk_box_pack_start (GTK_BOX(radio_vbox), radio_session, TRUE, TRUE, 0);
  gtk_widget_show (radio_session);

  radio_default =
    gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON(radio_session),
                                                 _("Save in the keyring"));
  gtk_box_pack_start (GTK_BOX(radio_vbox), radio_default, TRUE, TRUE, 0);
  gtk_widget_show (radio_default);

  g_signal_connect (G_OBJECT(check_button), "toggled",
                    G_CALLBACK(set_sensitivity_cb), radio_vbox);

  tmp = g_settings_get_string (gsettings, "save-keyring");
  if (tmp != NULL && g_strcmp0 (tmp, "default") == 0)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(radio_default), TRUE);
  g_free (tmp);

  g_object_unref (gsettings);
}
#endif

static gboolean
focus_out_cb (GtkWidget     *widget,
              GdkEventFocus *event,
              gpointer       data)
{
  GksuDialog *gksu_dialog = (GksuDialog *) data;

  if (gksu_dialog->priv->grab)
    {
      /* Make sure the window will always have the focus. */
      gtk_window_present (GTK_WINDOW(widget));
    }
  return TRUE;
}

static gboolean
entry_button_press_cb (GtkWidget      *widget,
                       GdkEventButton *event,
                       gpointer        data)
{
  GksuDialog *gksu_dialog = (GksuDialog *) data;

  /* Try to inhibit the popup menu which kills the grab. */
  if (gksu_dialog->priv->grab &&
      event->button == 3 && event->type == GDK_BUTTON_PRESS)
    {
      return TRUE;
    }

  return FALSE;
}

static void
gksu_dialog_init (GksuDialog *gksu_dialog)
{
  GksuDialogPrivate *priv;
  GtkWidget *hbox; /* aditional hbox for 'password: entry' label */
#if !GTK_CHECK_VERSION (3, 0, 0)
  GtkWidget *alignment;
#endif
  GdkKeymap *keymap;

  /* Initiate the private structure. */
  priv = G_TYPE_INSTANCE_GET_PRIVATE (gksu_dialog, GKSU_TYPE_DIALOG,
                                      GksuDialogPrivate);
  gksu_dialog->priv = priv;

  priv->main_vbox = gtk_dialog_get_content_area (GTK_DIALOG(gksu_dialog));

  gtk_container_set_border_width (GTK_CONTAINER(gksu_dialog), 6);
  gtk_box_set_spacing (GTK_BOX(priv->main_vbox), 12);

  g_signal_connect (GTK_WIDGET(gksu_dialog), "focus-out-event",
		    G_CALLBACK(focus_out_cb), gksu_dialog);
  g_signal_connect (GTK_WINDOW(gksu_dialog), "close",
                    G_CALLBACK (gtk_widget_hide), NULL);

  /* We "raise" the window because there is a race here for
   * focus-follow-mouse and auto-raise WMs that may put the window
   * in the background and confuse users.
   */
  gtk_window_set_keep_above (GTK_WINDOW(gksu_dialog), TRUE);

  /* the action buttons */
  /*  the cancel button  */
  priv->cancel_button = gtk_dialog_add_button (GTK_DIALOG(gksu_dialog),
                                               _("_Cancel"),
                                               GTK_RESPONSE_CANCEL);
  gtk_button_set_image (GTK_BUTTON(priv->cancel_button),
                        gtk_image_new_from_icon_name ("gtk-cancel",
                                                      GTK_ICON_SIZE_BUTTON));
  /*  the ok button  */
  priv->ok_button = gtk_dialog_add_button (GTK_DIALOG(gksu_dialog),
                                           _("_OK"),
                                           GTK_RESPONSE_OK);
  gtk_button_set_image (GTK_BUTTON(priv->ok_button),
                        gtk_image_new_from_icon_name ("gtk-ok",
                                                      GTK_ICON_SIZE_BUTTON));
  gtk_widget_grab_default (priv->ok_button);
  gtk_dialog_set_default_response (GTK_DIALOG(gksu_dialog), GTK_RESPONSE_OK);

  /* hbox */
#if GTK_CHECK_VERSION (3, 0, 0)
  priv->hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 12);
#else
  priv->hbox = gtk_hbox_new (FALSE, 12);
#endif
  gtk_container_set_border_width (GTK_CONTAINER(priv->hbox), 6);
  gtk_box_pack_start (GTK_BOX(priv->main_vbox), priv->hbox, TRUE, TRUE, 0);
  gtk_widget_show (priv->hbox);

  /* image */
  priv->image = gtk_image_new_from_icon_name ("dialog-password",
                                              GTK_ICON_SIZE_DIALOG);
#if GTK_CHECK_VERSION (3, 0, 0)
  gtk_widget_set_halign (priv->image, GTK_ALIGN_CENTER);
  gtk_widget_set_valign (priv->image, GTK_ALIGN_START);
#else
  gtk_misc_set_alignment (GTK_MISC(priv->image), 0.5, 0);
#endif
  gtk_box_pack_start (GTK_BOX(priv->hbox), priv->image, FALSE, FALSE, 0);
  gtk_widget_show (priv->image);

  /* vbox for label and entry */
#if GTK_CHECK_VERSION (3, 0, 0)
  priv->entry_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
#else
  priv->entry_vbox = gtk_vbox_new (FALSE, 2);
#endif
  gtk_box_pack_start (GTK_BOX(priv->hbox), priv->entry_vbox, FALSE, FALSE, 0);
  gtk_widget_show (priv->entry_vbox);

  /* label */
  priv->label = gtk_label_new (_("<span weight=\"bold\" size=\"larger\">"
                                 "Type the root password.</span>\n"));
  gtk_label_set_use_markup (GTK_LABEL(priv->label), TRUE);
  gtk_label_set_line_wrap (GTK_LABEL(priv->label), TRUE);
#if GTK_CHECK_VERSION (3, 16, 0)
  gtk_label_set_xalign (GTK_LABEL(priv->label), 0.0);
  gtk_label_set_yalign (GTK_LABEL(priv->label), 0.0);
#else
  gtk_misc_set_alignment (GTK_MISC(priv->label), 0.0, 0.0);
#endif
#if GTK_CHECK_VERSION (3, 0, 0)
  gtk_label_set_max_width_chars (GTK_LABEL(priv->label), 50);
  gtk_widget_set_margin_bottom (priv->label, 12);

  gtk_box_pack_start (GTK_BOX(priv->entry_vbox), priv->label, TRUE, TRUE, 0);
#else
  alignment = gtk_alignment_new (0.0, 0.0, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT(alignment), 0, 12, 0, 0);
  gtk_container_add (GTK_CONTAINER(alignment), priv->label);

  gtk_box_pack_start (GTK_BOX(priv->entry_vbox), alignment, TRUE, TRUE, 0);
  gtk_widget_show (alignment);
#endif
  gtk_widget_show (priv->label);

  /* alert */
  priv->alert = gtk_label_new (NULL);
  gtk_box_pack_start (GTK_BOX(priv->entry_vbox), priv->alert, TRUE, TRUE, 0);

  /* hbox for entry and label */
#if GTK_CHECK_VERSION (3, 0, 0)
  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);
#else
  hbox = gtk_hbox_new (FALSE, 2);
#endif
  gtk_box_pack_start (GTK_BOX (priv->entry_vbox), hbox,
		      TRUE, TRUE, 0);
  gtk_widget_show (hbox);

  /* entry label */
  priv->prompt_label = gtk_label_new (_("Password:"));
  gtk_box_pack_start (GTK_BOX(hbox), priv->prompt_label, FALSE, FALSE, 0);
  gtk_widget_show (priv->prompt_label);

  /* entry */
  priv->entry = gtk_entry_new ();
  g_signal_connect (GTK_ENTRY(priv->entry), "button-press-event",
                    G_CALLBACK(entry_button_press_cb), gksu_dialog);
  g_signal_connect_swapped (GTK_ENTRY(priv->entry), "activate",
                            G_CALLBACK(gtk_button_clicked), priv->ok_button);
#if GTK_CHECK_VERSION (3, 6, 0)
  gtk_entry_set_input_purpose (GTK_ENTRY(priv->entry),
                               GTK_INPUT_PURPOSE_PASSWORD);
#endif
  gtk_entry_set_visibility (GTK_ENTRY(priv->entry), FALSE);

  if (gtk_entry_get_invisible_char (GTK_ENTRY(priv->entry)) == '*')
    {
      gtk_entry_set_invisible_char (GTK_ENTRY(priv->entry), 0x25CF);
    }
  gtk_box_pack_start (GTK_BOX (hbox), priv->entry, TRUE, TRUE, 0);
  gtk_widget_show (priv->entry);
  gtk_widget_grab_focus (priv->entry);

  /* label capslock warning */
  priv->label_warn_capslock = gtk_label_new ("");
  gtk_widget_show (priv->label_warn_capslock);
  gtk_label_set_justify (GTK_LABEL(priv->label_warn_capslock),
			 GTK_JUSTIFY_CENTER);
  gtk_label_set_use_markup (GTK_LABEL(priv->label_warn_capslock), TRUE);
  gtk_box_pack_start (GTK_BOX(priv->entry_vbox), priv->label_warn_capslock,
                      TRUE, TRUE, 0);

  /* Monitor buttons being pressed to see if Caps Lock is pressed. */
  keymap =
    gdk_keymap_get_for_display (gtk_widget_get_display (GTK_WIDGET(gksu_dialog)));
  g_signal_connect (G_OBJECT(keymap), "state-changed",
		    G_CALLBACK(verify_capslock_cb), (gpointer) gksu_dialog);
}

/**
 * gksu_dialog_new:
 * @grab: TRUE or FALSE
 * @sudo_mode: TRUE or FALSE
 *
 * Creates a new #GksuDialog.
 * Free with g_object_unref() when done.
 *
 * Returns: (transfer full): the new #GksuDialog
 */
GtkWidget *
gksu_dialog_new (gboolean grab,
                 gboolean sudo_mode)
{
  GtkWidget *dialog;
  GtkWindowType type = GTK_WINDOW_TOPLEVEL;
#if !GTK_CHECK_VERSION (3, 16, 0)
  GdkPixbuf *icon;
#endif

  if (grab)
    type = GTK_WINDOW_POPUP;

#if !GTK_CHECK_VERSION (3, 16, 0)
  /* This way the "icon too large" warning won't be emitted. */
  icon = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
                                   "applications-system", 48,
                                   GTK_ICON_LOOKUP_USE_BUILTIN,
                                   NULL);

#endif
  dialog = GTK_WIDGET(g_object_new (GKSU_TYPE_DIALOG,
                                    "type", type,
                                    "modal", TRUE,
                                    "resizable", FALSE,
                                    "title", _("Granting Rights"),
#if GTK_CHECK_VERSION (3, 16, 0)
                                    "icon-name", "applications-system",
#else
                                    "icon", icon,
#endif
                                    "grab", grab,
                                    "sudo_mode", sudo_mode,
                                    NULL));
#if !GTK_CHECK_VERSION (3, 16, 0)
  g_object_unref (icon);
#endif

  gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);

  return dialog;
}

/**
 * gksu_dialog_set_message:
 * @dialog: the dialogue on which to set the message
 * @message: (not nullable): the message to be set on the dialogue
 *
 * Sets the message that is displayed to the user when requesting a
 * password. You can use Pango markup to modify font attributes.
 */
void
gksu_dialog_set_message (GksuDialog *dialog, const char *message)
{
  GtkWidget *label = dialog->priv->label;

  g_return_if_fail (message != NULL);

  gtk_label_set_markup (GTK_LABEL(label), message);
}

/**
 * gksu_dialog_get_message:
 * @dialog: the dialogue from which to get the message
 *
 * Gets the current message that the dialogue will use when run.
 *
 * Returns: (transfer none): a pointer to the string containing the
 * message; you need to make a copy of the string to keep it
 */
const char *
gksu_dialog_get_message (GksuDialog *dialog)
{
  GtkWidget *label = dialog->priv->label;

  return gtk_label_get_text (GTK_LABEL(label));
}

/**
 * gksu_dialog_set_alert:
 * @dialog: the dialogue on which to set the alert
 * @alert: (not nullable): the alert to be set on the dialogue
 *
 * Sets the alert that is displayed to the user when requesting a
 * password. You can use Pango markup to modify font attributes.
 * This alert should be used to display messages like
 * "wrong password" on password retry, for example.
 */
void
gksu_dialog_set_alert (GksuDialog *dialog, const char *alert)
{
  GtkWidget *label = dialog->priv->alert;

  g_return_if_fail (alert != NULL);

  gtk_label_set_markup (GTK_LABEL(label), alert);
}

/**
 * gksu_dialog_get_alert:
 * @dialog: the dialogue from which to get the alert
 *
 * Gets the current alert that the dialogue will use
 * when run.
 *
 * Returns: (transfer none): a pointer to the string containing the
 * alert; you need to make a copy of the string to keep it
 */
const char *
gksu_dialog_get_alert (GksuDialog *dialog)
{
  GtkWidget *label = dialog->priv->alert;

  return gtk_label_get_text (GTK_LABEL(label));
}

/**
 * gksu_dialog_set_icon:
 * @dialog: the dialogue on which the icon will be set
 * @icon: (nullable): a #GdkPixbuf from which to set the image
 *
 * Sets the icon that will be shown on the dialogue. Should
 * probably not be used, as the default icon is the default
 * authorisation icon.
 */
void
gksu_dialog_set_icon (GksuDialog *dialog, GdkPixbuf *icon)
{
  GtkWidget *image = dialog->priv->image;

  gtk_image_set_from_pixbuf (GTK_IMAGE(image), icon);
}

/**
 * gksu_dialog_get_icon:
 * @dialog: the dialogue from which the icon should be got
 *
 * Gets the #GtkImage which is currently defined as the icon for
 * the authorisation dialogue.
 *
 * Returns: (transfer none): a #GtkWidget which is the #GtkImage
 */
GtkWidget *
gksu_dialog_get_icon (GksuDialog *dialog)
{
  return dialog->priv->image;
}

/**
 * gksu_dialog_get_password:
 * @dialog: the dialogue from which to get the message
 *
 * Gets the password typed by the user on the dialogue.
 * This is a convenience function to grab the password easily from
 * the dialogue after calling gtk_dialog_run().
 *
 * Returns: (transfer full): a newly allocated string containing
 * the password
 */
char *
gksu_dialog_get_password (GksuDialog *dialog)
{
  GtkEditable *entry = GTK_EDITABLE(dialog->priv->entry);

  return gtk_editable_get_chars (entry, 0, -1);
}

/**
 * gksu_dialog_set_prompt:
 * @dialog: the dialogue on which to set the prompt
 * @prompt: (not nullable): the prompt to be set on the dialogue
 *
 * Sets the prompt that is displayed to the user when requesting a
 * password. You can use Pango markup to modify font attributes.
 */
void
gksu_dialog_set_prompt (GksuDialog *dialog, const char *prompt)
{
  GtkWidget *label = dialog->priv->prompt_label;

  g_return_if_fail (prompt != NULL);

  gtk_label_set_markup (GTK_LABEL(label), prompt);
}

/**
 * gksu_dialog_get_prompt:
 * @dialog: the dialogue from which to get the prompt
 *
 * Gets the current prompt that the dialogue will use when run.
 *
 * Returns: a pointer to the string containing the prompt; you need
 * to make a copy of the string to keep it
 */
const char *
gksu_dialog_get_prompt (GksuDialog *dialog)
{
  GtkWidget *label = dialog->priv->prompt_label;

  return gtk_label_get_text (GTK_LABEL(label));
}
