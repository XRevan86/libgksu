/*
 * libgksu -- a library providing access to su functionality
 * Copyright (C) 2004-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <https://gnu.org/licenses>.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>

#include <glib.h>
#include <glib/gstdio.h>

#include "defines.h"

#include "../config.h"

static void
strip (char *string)
{
  if (string[strlen(string) - 1] == '\n')
    string[strlen(string) - 1] = '\0';
}

/**
 * clean_dir:
 * @dirname: the temporary directory created by gksu for xauth
 *
 * Removes the temporary directory created to hold the X authorization
 * file, and of course, the file itself.
 */
static void
clean_dir (const char *dirname)
{
  char *xauthname;
  
  xauthname = g_strdup_printf ("%s/.Xauthority", dirname);
  g_unlink (xauthname);
  g_free (xauthname);

  if (g_rmdir (dirname) != 0)
    g_fprintf (stderr, "ERROR: unable to remove directory %s: %s",
               dirname, g_strerror (errno));
}

static void
read_gstring_from_stdin(GString *s)
{
  char buffer[255];
  char *readp;
  do
  {
    readp = fgets (buffer, sizeof (buffer), stdin);
    if (readp == NULL)
      return;
    strip (buffer);
    g_string_append (s, buffer);
  } while (sizeof (buffer) - 1 == strlen (readp));
}

int
main (int argc, char **argv)
{
  GString *s;
  char *xauth_bin, *xauth_dir, *xauth_file;
  char *xauth_display, *xauth_token;
  int exit_code = 0;
  int child_exit_code = 0;

  if (argc < 2)
    {
      g_fprintf (stderr, "gksu: command missing");
      return 1;
    }

  xauth_dir = g_dir_make_tmp (PACKAGE_NAME "-XXXXXX", NULL);
  if (xauth_dir == NULL)
    {
      g_fprintf (stderr, "gksu: failed creating xauth_dir\n");
      return 1;
    }

  g_fprintf (stderr, "gksu: waiting\n");

  xauth_file = g_strdup_printf ("%s/.Xauthority", xauth_dir);
  g_setenv ("XAUTHORITY", xauth_file, TRUE);

  s = g_string_sized_new (255);
  read_gstring_from_stdin (s);

  /* strlen ("gksu-run: ") == 10, see libgksu.c */
  xauth_display = g_strdup (s->str + 10);

  s = g_string_truncate (s, 0);
  read_gstring_from_stdin (s);

  xauth_token = g_strdup_printf ("%s", s->str + 10);

  /* Cleanup the environment; some variables we bring from the user
   * environment make dconf-based applications misbehaving these days.
   */
  g_unsetenv ("DBUS_SESSION_BUS_ADDRESS");
  g_unsetenv ("XDG_RUNTIME_DIR");
  g_unsetenv ("ORBIT_SOCKETDIR");

  /* Find out where the xauth binary is located. */
  xauth_bin = g_find_program_in_path ("xauth");
  if (xauth_bin == NULL)
    {
      if (g_file_test ("/usr/bin/xauth", G_FILE_TEST_IS_EXECUTABLE))
        xauth_bin = g_strdup ("/usr/bin/xauth");
      else if (g_file_test ("/usr/X11R6/bin/xauth", G_FILE_TEST_IS_EXECUTABLE))
        xauth_bin = g_strdup ("/usr/X11R6/bin/xauth");
      else
        {
          g_fprintf (stderr,
                     _("Failed to obtain xauth key: xauth binary not found "
                       "at usual locations"));
          exit_code = 1;
          goto quit;
        }
    }

  {
    char **command;
    char *command_xauth;

    command_xauth = g_strdup_printf ("%s add %s . \"%s\"",
                                     xauth_bin, xauth_display, xauth_token);
    if (g_shell_parse_argv (command_xauth, NULL, &command, NULL))
      {
        g_spawn_sync (NULL, command, NULL,
                      G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL,
                      NULL, NULL, NULL, NULL,
                      NULL, NULL);
        g_strfreev (command);
      }
    g_free (command_xauth);

    if (g_shell_parse_argv (argv[1], NULL, &command, NULL))
      {
        g_spawn_sync (NULL, command, NULL,
                      G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, NULL,
                      &child_exit_code, NULL);
        g_strfreev (command);

        if (WIFEXITED(child_exit_code))
          exit_code = WEXITSTATUS(child_exit_code);
        else if (WIFSIGNALED(child_exit_code))
          exit_code = -1;
      }
    else
      exit_code = -1;
  }

quit:
  clean_dir (xauth_dir);

  g_free (xauth_dir);
  g_free (xauth_display);
  g_free (xauth_file);
  g_free (xauth_bin);
  g_string_free (s, TRUE);

  return exit_code;
}
