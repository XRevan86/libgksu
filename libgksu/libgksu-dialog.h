/*
 * libgksu-dialog -- GTK+ widget and convenience functions
 * Copyright (C) 2004-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <https://gnu.org/licenses>.
 */

#ifndef __LIBGKSU_DIALOG_H__
#define __LIBGKSU_DIALOG_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GKSU_TYPE_DIALOG (gksu_dialog_get_type ())
#define GKSU_DIALOG(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), GKSU_TYPE_DIALOG, GksuDialog))
#define GKSU_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), GKSU_TYPE_DIALOG, GksuDialogClass))
#define GKSU_IS_DIALOG(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GKSU_TYPE_DIALOG))
#define GKSU_IS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GKSU_TYPE_CONTEXT))
#define GKSU_DIALOG_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GKSU_TYPE_DIALOG, GksuDialogClass))

typedef struct _GksuDialog GksuDialog;
typedef struct _GksuDialogPrivate GksuDialogPrivate;
typedef struct _GksuDialogClass GksuDialogClass;

/**
 * GksuDialogClass:
 * @parent_class: The parent class.
 */
struct _GksuDialogClass
{
  GtkDialogClass parent_class;
};

/**
 * GksuDialog:
 * @dialog: parent widget
 *
 * Convenience widget based on #GtkDialog to request a password.
 */
struct _GksuDialog
{
  GtkDialog dialog;

  /*< private >*/
  GksuDialogPrivate *priv;
};

GType
gksu_dialog_get_type (void);

GtkWidget *
gksu_dialog_new (gboolean grab,
                 gboolean sudo_mode);

void
gksu_dialog_set_message (GksuDialog *dialog, const char *message);

const char *
gksu_dialog_get_message (GksuDialog *dialog);

void
gksu_dialog_set_alert (GksuDialog *dialog, const char *alert);

const char *
gksu_dialog_get_alert (GksuDialog *dialog);

void
gksu_dialog_set_icon (GksuDialog *dialog, GdkPixbuf *icon);

GtkWidget *
gksu_dialog_get_icon (GksuDialog *dialog);

char *
gksu_dialog_get_password (GksuDialog *dialog);

void
gksu_dialog_set_prompt (GksuDialog *dialog, const char *prompt);

const char *
gksu_dialog_get_prompt (GksuDialog *dialog);

G_END_DECLS

#endif
