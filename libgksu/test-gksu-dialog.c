/*
 * libgksu-dialog -- GTK+ widget and convenience functions
 * Copyright (C) 2004-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <https://gnu.org/licenses>.
 */

#include <glib/gstdio.h>
#include <gtk/gtk.h>

#include "libgksu-dialog.h"

int
main (int argc, char **argv)
{
  GtkWidget *gksu_dialog;
  GdkPixbuf *pixbuf;
  int response;
  char *password;

  gtk_init (&argc, &argv);

  gksu_dialog = gksu_dialog_new (FALSE);
  gtk_window_set_title (GTK_WINDOW(gksu_dialog), "My test!");
  gksu_dialog_set_message (GKSU_DIALOG(gksu_dialog),
                           "<b>Gimme the damn password, luser!</b>");
  pixbuf = gdk_pixbuf_new_from_file ("/usr/share/pixmaps/apple-green.png",
				     NULL);
  gksu_dialog_set_icon (GKSU_DIALOG(gksu_dialog), pixbuf);

  gtk_widget_show_all (gksu_dialog);

  response = gtk_dialog_run (GTK_DIALOG(gksu_dialog));
  g_fprintf (stderr, "response ID: %d\n", response);

  password = gksu_dialog_get_password (GKSU_DIALOG(gksu_dialog));
  g_fprintf (stderr, "password: %s\n", password);

  gtk_widget_hide (gksu_dialog);
  while (gtk_events_pending ())
    gtk_main_iteration_do (FALSE);

  return 0;
}
