/*
 * libgksu -- a library providing access to su functionality
 * Copyright (C) 2004-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <https://gnu.org/licenses>.
 */

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include <glib/gstdio.h>

#include "gksu.h"

/* A "stolen" from musl getpass(). */
char *
su_ask_pass (GksuContext *context, const char *prompt,
	     gpointer data, GError **error)
{
  int fd;
  struct termios s, t;
  ssize_t l;
  static char password[128];

  if ((fd = g_open ("/dev/tty", O_RDWR | O_NOCTTY)) < 0)
    return NULL;

  tcgetattr (fd, &t);
  s = t;
  t.c_lflag &= ~(ECHO | ISIG);
  t.c_lflag |= ICANON;
  t.c_iflag &= ~(INLCR | IGNCR);
  t.c_iflag |= ICRNL;
  tcsetattr (fd, TCSAFLUSH, &t);
#ifndef __FreeBSD_kernel__
  tcdrain (fd);
#endif

  if (write (fd, prompt, strlen (prompt)) < 0)
    g_fprintf (stderr, "su_ask_pass failed to write data\n");

  l = read (fd, password, sizeof (password));
  if (l >= 0)
    {
      if (l > 0 && password[l - 1] == '\n')
        --l;
      password[l] = 0;
    }

  tcsetattr (fd, TCSAFLUSH, &s);

  if (write (fd, "\n", 1))
    g_fprintf (stderr, "su_ask_pass failed to write data\n");

  close (fd);
  return l < 0 ? NULL : password;
}

void
password_not_needed (GksuContext *context,
		     gpointer data)
{
  fprintf (stderr, "Will run %s as %s!\n",
           gksu_context_get_command (context),
           gksu_context_get_user (context));
}

int
main (int argc, char **argv)
{
  GksuContext *context;
  GError *error = NULL;
  gboolean try_su = TRUE;
  gboolean try_sudo = TRUE;
  gboolean try_run = TRUE;
  char *command;

  if (argc > 1)
    {
      try_su = try_sudo = try_run = FALSE;
      if (g_strcmp0 (argv[1], "--su") == 0)
        try_su = TRUE;
      else if (g_strcmp0 (argv[1], "--sudo") == 0)
        try_sudo = TRUE;
      else if (g_strcmp0 (argv[1], "--run") == 0)
        try_run = TRUE;
    }

  gtk_init (&argc, &argv);

  context = gksu_context_new ();
  command = g_find_program_in_path ("xterm");

  if (command == NULL)
    {
      g_free (context);
      return 1;
    }

  gksu_context_set_debug (context, TRUE);
  gksu_context_set_command (context, command);

  if (try_su)
    {
      g_printf ("Testing gksu_su...\n");
      gksu_su (command, &error);
      if (error)
        g_fprintf (stderr, "gksu_su failed: %s\n", error->message);

      error = NULL;
      g_printf ("Testing gksu_su_full...\n");
      gksu_su_full (context,
		    su_ask_pass, NULL,
		    password_not_needed, NULL,
		    NULL, &error);
    }

  /* of course you need to set up /etc/sudoers for this to work */
  if (try_sudo)
    {
      g_printf ("Testing gksu_sudo...\n");
      error = NULL;
      gksu_sudo (command, &error);
      if (error)
        g_fprintf (stderr, "gksu_sudo failed: %s\n", error->message);

      g_printf ("Testing gksu_sudo_full...\n");
      error = NULL;
      gksu_sudo_full (context,
		      su_ask_pass, NULL,
		      password_not_needed, NULL,
		      NULL, &error);
      if (error)
        g_fprintf (stderr, "gksu_sudo_full failed: %s\n", error->message);
    }

  if (try_run)
    {
      g_printf ("Testing gksu_run...\n");
      error = NULL;
      gksu_run (command, &error);
      if (error)
        g_fprintf (stderr, "gksu_run failed: %s\n", error->message);

      g_printf ("Testing gksu_run_full...\n");
      error = NULL;
      gksu_run_full (context,
                     su_ask_pass, NULL,
                     password_not_needed, NULL,
                     NULL, &error);
      if (error)
        g_fprintf (stderr, "gksu_run_full failed: %s\n", error->message);
    }

  g_free (command);
  gksu_context_unref (context);
  return 0;
}

