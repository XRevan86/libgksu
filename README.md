# [libgksu](https://gitlab.com/XRevan86/libgksu), a library from gksu

It provides a simple API to use su and sudo in applications that need to
execute tasks as an other user. It provides X authentication facilities for
running applications in an X session.

Reports are very welcome at gksu's [GitLab Issues](https://gitlab.com/XRevan86/gksu/issues).
