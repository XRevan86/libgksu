Release 2.0.12 (2009-06-29 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/libgksu.c:
  - Fix for double free.
    (https://bugs.debian.org/534844)

gksu-properties/gksu-properties.c, gksu-properties/Makefile.am,
configure.ac:
  - Use GtkBuilder instead of libglade.

libgksu/gksu-run-helper.c:
  - Fix the fact that the main function ends without returning a
    value, on success (doh).

Release 2.0.11 (2009-06-24 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Should work with sudo when requiretty is on.
(https://bugs.debian.org/531477)

Make su code work on Red Hat systems.

Release 2.0.10 (2009-05-09 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Add a manpage for gksu-properties.
(https://bugs.debian.org/497591)

Fix the off-by-one error and use of uninitialized memory.
(https://bugs.debian.org/525876)

Release 2.0.9 (2009-03-04 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/libgksu.c:
  - Fix how the GdkDisplay* is obtained at
    gksu_context_launch_initiate().

libgksu/gksu-run-helper.c:
  - Cleanup the environment from ORBIT_SOCKETDIR and
    DBUS_SESSION_BUS_ADDRESS to acommodate recent changes in gconf.

Release 2.0.8 (2009-02-22 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/gksu-run-helper.c:
  - Fix very long commands not fitting in the helper's
    statically-sized buffer, thanks to Michael Vogt.
    (https://bugs.debian.org/486087,
     https://bugs.launchpad.net/173757)

libgksu/libgksu.c:
  - Fix sudo having problems resolving network addresses breaking
    gksu, thanks to Michael Vogt.
    (https://bugs.debian.org/486083,
     https://bugs.launchpad.net/237325)
  - Allow changing the location of the lockfile, so that libgksu
    will work on AFS/NFS mounted home directories, thanks to
    Daniel Richard G. <skunk@iSKUNK.ORG>.
 - Add threads protection for the gdk fadeout/fadein animation, so
   that callers will not have X errors when trying to use gdk
   functions in parallel with us.
   (http://bugzilla.gnome.org/549760)

configure.ac, libgksu/libgksu.{c,h}, libgksu/gksu-run-helper.c:
  - Return the same status code as the child, thanks to
    Joshua Kwan <jkwan@vmware.com>.

configure.ac:
  - Add th to ALL_LINGUAS.

Release 2.0.7 (2008-05-27 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix for early return of gksu_sudo_full.
[Ben Hutchings <ben@decadent.org.uk>]
(https://bugs.debian.org/463709)

Fix detection for the -version-script option.

Release 2.0.6 (2008-05-16 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix the way gksu waits for the child process to finish, thanks to
Michael Vogt.
(https://bugs.launchpad.net/121581)

Fix user and command order on the message that is displayed when a
user other than root is targeted.
(https://bugs.debian.org/449180)

Add a way to always ask for password.
(https://bugs.debian.org/453423)

Fix capslock notification.
(https://bugs.debian.org/453426)

Update translations.

Release 2.0.5 (2007-06-10 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Use a less dumb way of relaying child's stdout/stderr, thus not
requiring so many wakeups.
(https://bugs.debian.org/425679)

Complete startup notification when cancel is pressed.
(https://bugs.debian.org/426495)

Always try creating the keyring when saving the password; if the
keyring already exists the creation is a no-op, so this is safe.
(https://bugs.debian.org/412681)

configure.ac:
  - Add ko, sv and zh_CN to ALL_LINGUAS.
    (https://bugs.debian.org/416186)

Release 2.0.4 (2007-02-04 Gustavo Noronha Silva <kov@debian.org>)
===================================================================
The "Apaziguooo" Release

Make sure we remove the \n from the password we got from the
keyring, if it exists.
(Chttps://bugs.debian.org/395462)

Fix the desktop startup notification variable not being set; sudo
seems to unset the variable anyway, will talk to sudo's maintainer.
(https://bugs.debian.org/401268)

Make libgksu only replace the 'password typing' character when '*'
is the default one, thanks to Vitali Ishenko <betalb@gmail.com>.

Update translations.

Release 2.0.3 (2006-10-23 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/libgksu.c:
  - Make sure the dialog is _destroyed_ instead of just being
    hidden, after some action has been taken in the dialog that
    asks for the password.
  - Return WRONGAUTOPASS instead of WRONGPASS when wrong password
    is the one we got from the keyring.
  - Store the password in the keyring without the \n.
    (https://bugs.debian.org/394895)

Release 2.0.2 (2006-10-22 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Do not allow losing focus because of the context menu.
(https://bugs.debian.org/391804)

Make detecting the need for a password when running su hopefully
more robust, lowering the possibility of a deadlock.

Release 2.0.1 (2006-10-08 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/libgksu.c:
  - If xauth is an empty string when using the su backend, then try
    to get it again by striping the hostname part of the display
    string; this is needed for ssh X forwarding.
    (https://bugs.debian.org/389254)
  - Use some brute force reading when using sudo, to be able to
    handle stuff like the sudo lecture and pam modules debugging.
    (https://bugs.debian.org/391803)

Release 2.0.0 (2006-09-23 Gustavo Noronha Silva <kov@debian.org>)
===================================================================
The "official" 2.0 Release

configure.ac:
  - Add lt, it to ALL_LINGUAS.

libgksu/libgksu.c:
  - Add a gettext call in the prompt in su_ask_password(), so that
    the prompt is translated.
  - Fix the detection of sudo password failure by making the fd
    block while expecting the line right after the password is sent
    to sudo.
  - Have gksu ignore lines coming from the process 'till it is able
    to find the gksu: waiting line; this is done observing some
    time and size constraints, though, so we won't end up in an
    infinite loop; libpam-mount should now work happily with debug
    on.
    (https://savannah.nongnu.org/bugs/?17139,
     https://bugs.debian.org/386213)
  - Also make su's child session leader, like sudo's, so that when
    the program you ran launches another, closing the first will
    not destroy the second.

Release 1.9.8 (2006-08-06 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/libgksu.c:
  - Bring the rest of the startup notification code from the
    application.

Release 1.9.7 (2006-07-19 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix a regression introduced in last version, in which the keyring
 checkboxes would never appear, even for su mode.

Release 1.9.6 (2006-07-17 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksuui/gksuui-dialog.c, libgksu/libgksu.c:
  - Add a new object property to GksuuiDialog, and pass the
    sudo_mode boolean to it on object instanciation, so that the
    sudo window will not have the gnome_keyring stuff, again.

Fix gksudo some times failing because the prompt was delayed a bit.
(https://bugs.debian.org/377746)

Hopefully fix gksu failing when DISPLAY has a hostname.
(https://bugs.debian.org/349652)

Release 1.9.5 (2006-07-12 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

gksu-properties/*:
  - Start a simple capplet to configure the gksu gconf preferences.

libgksu/defines.h:
  - Small adaption so it can better be used by gksu-properties.

libgksu/libgksu.c:
  - Remove \n from the check of the command name, since gtop does
    not give a \n-terminated string, thanks to
    Benoît Dejean <benoit@placenet.org> for pointing this out.

gksu.png, gksu-properties/{Makefile.am,gksu-properties.desktop.in,
gksu-properties.c}:
  - Add an icon, and a desktop file so that the capplet will show
    in the preferences menu for the desktop.

* libgksu/libgksu.{c,h}, libgksuui/gksu-dialog.{c,h}:
  - Implement a new 'alert' label, to allow applications showing
    messages like 'wrong password, try again'.

libgksu/libgksu.{c,h}:
  - Add accessor methods for the startup notification launcher
    context.

Release 1.9.4 (2006-06-26 Gustavo Noronha Silva <kov@debian.org>)
===================================================================
Brown paperbag Release

Fix pointer derreferencing which was making libgksu not work at all.

Release 1.9.3 (2006-06-23 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

configure.ac, libgksu/Makefile.am:
  - Only use the version script if the system we're building on
    supports it, thanks to Daniel Macks.

libgksu/libgksu.c:
  - Check fread's return value before using strlen and setting a \0
    in the xauth string, to prevent problems when xauth returns
    nothing.
    (https://savannah.nongnu.org/bugs/?7698)
  - Handle locales which mess with the 'su:' layout.
  - Use TRUE for success and FALSE for failure, to match glib's
    spawn API - well, it's a development release =D.
  - Handle canceling the dialog correctly; added a new error to
    the enum.

libgksu/libgksu.c,configure.ac:
  - Get the process name in a platform independent way, thanks to
    Benoît Dejean <benoit@placenet.org>.

Add a French translation.

Release 1.9.2 (2006-05-01 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/libgksu.c:
  - Use the internal context->sudo_mode to tell the ask_pass
    function if we're on su or sudo mode.
  - Provide an API for simply getting the password.
  - Standardize the get methods that return string to return NULL
    if they were not set, and updated "docstrings" to fit.
  - Remove translatable marks for strings that are errors printed
    to stderr.
  - Made some strings look better or more HIG compliant.

libgksu/libgksu.{c.h}:
  - Implement the message setting and getting functions, and use
    the message that is given when creating the dialog.

libgksu/Makefile.am:
  - Increase a minor version of the library, since some symbols
    were added.

libgksuui/Makefile.am:
  - Do not build a static version of the lib only;
    I need a shared version with PIC symbols built so that libgksu
    will be able to be fully PIC.

libgksuui/libgksu.{c,h}, libgksuui/gksuui-dialog.c:
  - Accept changes from the Ubuntu people to make the dialog and
    messages be more beatiful; also provide an API that enables
    specifying a descriptive name for the command, that is used
    automatically in the message.

libgksuui/gksuui-dialog.c:
  - Do not center the label that is used to display the message.

docs/*:
  - Update documentation.

configure.ac:
  - Fix a gettext domain define.

Release 1.9.1 (2006-04-28 Gustavo Noronha Silva <kov@debian.org>)
===================================================================
Brown paperbag Release

Fix the pkg-config dependency information.

Release 1.9.0 (2006-04-23 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Major design overhaul:
  - Incorporate libgksuui.
  - No longer try to be UI independent; use GTK+ mercylessly.
  - Incorporate gconf schema and settings handling.
  - Massive API review, removal of most public functions.
  - GksuContext is no longer a GObject-based object, no longer has
    many accessor functions for its data fields and is needed for
    the _full versions of the main API functions right now; this
    allows saner handling of password prompting and housekeeping.

gksu.schemas.in:
  - Remove always-ask-password for now, it will probably be
    replaced by something else when gnome-keyring is integrated.

intltoolized, modified some stuff for the package renaming to work.

Renaming files:
  - libgksu/gksu-context.{c,h} → libgksu/libgksu.{c,h}.

libgksu/libgksu.c:
  - Integrate 'prompt before grab' support; also make the
    force-grab configuration effective.
  - Add accessor methods for the GksuContext structure, I want it
    to be opaque.
  - Preliminary support for the keyring imported from the gksu
    application code; it now removes the password from the keyring
    in case it tries with it and fails.
  - Avoid buffer overflows on the debug code, thanks to
    Benoît Dejean <benoit@placenet.org>.
  - Fix a corner condition which happens when fgets returns badly
    with a specific application, thanks to Michael Vogt.
  - Use a non-blocking read when looking for the sudo prompt, so
    that we won't get blocked if there is none; also, make sure the
    window is displayed.
  - (su) Track that the keyring was used to enhance the
    pass_not_needed() display logic; display it parallel to running
    the command, to match the gksu_sudo_full() behavior.
  - Only print the password when on debug mode.
  - Init sn_context with NULL, so non-SN-supporting users will not
    segfault when we check for the value.

libgksu/libgksu.{c,h}:
  - New gksu_run{,_full}() API, that is a generic way of using gksu
    functionality, leaving the choice of su or sudo backend to the
    gconf setting.

libgksu/libgksu.c, docs/*:
  - Revamp documentation; gtk-doc-style comments added to
    functions and some fixes to return values were made to match
    the docs.

libgksuui/gksuui-dialog.{c,h}:
  - Report that capslock is enabled, thanks to Michael Vogt.
  - Add an UI for the keyring support in the main widget.

libgksu/libgksu.c, configure.ac:
  - Support startup notfication, thanks Michael Vogt.

libgksu/libgksu.c, libgksu/gksu-run-helper.c, libgksu/test-gksu.c:
  - Find the new location of the xauth binary while still being
    able to work with the old one.
    (https://bugs.debian.org/362118,
     https://bugs.debian.org/362125,
     https://bugs.debian.org/362224)

libgksu/Makefile.am, libgksu/libgksu2.pc.in:
  - Misc fixes to the build system.

libgksu/test-gksu.c:
  - Display a message before testing gksu_sudo_full(), so we get a
    better picture of where stuff is happening.
  - Add a test for the new gksu_run{,_full}() API.

Release 1.3.7 (2005-11-23 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix some bugs with long-standing processes using the library.
Thanks to Benoît Dejean <benoit@placenet.org> for testing and help.

Release 1.3.6 (2005-10-25 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix a logic problem which broke usage of pam_wheel again.
(https://bugs.debian.org/331124)

Release 1.3.5 (2005-09-29 Gustavo Noronha Silva <kov@debian.org>)
===================================================================
Brown paperbag release

Fix some memory leaks.

Release 1.3.4 (2005-09-28 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/gksu-context.{c,h}:
  - Improve error reporting for sudo; tries to detect if sudo
    reported that the user cannot run the command for that user.
    (https://bugs.debian.org/295298)

libgksu/gksu-context.c:
  - Treat the command line to remove the quotes that may have been
    added by gksu's multi-argument processing code.
  - Use /proc information to find out the name of the current child
    process to check if it was sudo who returned a failure code or
    the program we called on Linux systems; patch and modifications
    done to it were made by Michael Vogt and his advice, respectively.
  - Handle 'not in sudoers' message.
  - Apply the same solution to handling error return checking to
    the su backend.

Relibtoolized with libtool --automake -c -f
(Debian's libtool 1.5.20-1).

Add French translation.
(https://bugs.debian.org/323589)

Release 1.3.3 (2005-08-03 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/gksu-context.{c,h}, libgksu/test-gksu.c:
  - Implement the gksu_context_ask_and_run() counterpart for sudo.

libgksu/gksu-context.c, libgksu/Makefile.am, configure.ac:
  - Revert inclusion of the keyring support, which should actually
    go into the application, not the library.

libgksu/gksu-context.{c,h}:
  - Replace ask_and_run with run_full for both su and sudo; the new
    API function which accepts another callback function that
    allows the user application to do something if a password was
    not needed like, for example, warning the user.
  - Leave ask_and_run as deprecated functions to let software which
    used it at some point in development keep building and running.

Release 1.3.2 (2005-07-14 Gustavo Noronha Silva <kov@debian.org>)
===================================================================
The "Helsinki" Release

libgksu/gksu-context.c:
  - Make a gksu_context_ask_and_run() that receives a function
    pointer to request password if needed (so you don't have to
    fill context->password beforehand).
  - Kill sudo when trying validation and wait a bit before
    wait()'ing, so gksu gets blocked for less time.
  - Better checking of whether a prompt is waiting for a password
    or not to figure out if a password is needed or not; this
    enhances the 'only ask for password if really needed' approach
    and is one more step on addressing
    https://bugs.debian.org/246652; this required some tweaking to
    the logic used in conversation with gksu-run-helper.
  - Fix wording on xauth warning message so it will not use first
    person.
    (https://bugs.debian.org/309563)
  - Also use the ECHO trick for try_su_run().
  - The call to usleep must be inside the while loop so gksu won't
    be eating CPU time.
  - Kill su in try_su_run instead of simply sending a "\n" if it
    asks for password, so we improve speed and avoid dead locks.
  - After-password conversation with helper improved quite a bit
    and made simpler; should fix some problems with gksu-run:
    strings coming out eventually and prevent some cases of dead
    locks.
  - Move the keyring_create_item_cb outside of the function which
    calls it, so gksu will not segfault on x86-64, which does not
    permit execution of the stack, thanks to Aurelien Jacobs
    <aurel@gnuage.org>.
    (https://bugs.debian.org/318031, https://bugs.debian.org/307975,
     https://bugs.debian.org/314369)
  - Wrap the keyring callback in the ENABLE_GNOME_KEYRING ifdef.
  - Small fixes and improvements to some info and error messages.
  - Handle the dropping of the \n echoed after password was given
    if a password needed to be given, so we have wheel trust and
    other situations in which you don't need a password handled
    correctly.

libgksu/gksu-context.{c,h}:
  - Modify the password-asking function to receive a GError** so we
    have the possibility of creating a 'problem reporting'
    interface for it.
  - Also, check the return value of the function and bail out if it
    is FALSE.

libgksu/test-gksu.c:
  - Test gksu_context_ask_and_run.
  - Fix su_ask_pass declaration to include the GError** argument.

libgksu/gksu-context.c, docs/libgksu1.2-sections.txt:
  - Make sure every public API function is documented.

Release 1.3.1 (2005-06-18 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/test-gksu.c:
  - Use the new API function for testing if a sudo password is
    needed.
  - Accept --su and --sudo arguments to try only su or sudo
    respectively.

libgksu/gksu-context.{c,h}:
  - Add gksu_context_sudo_try_need_password() to pre-check if we
    need a password; this uses sudo -v for now but will
    incorporate other ways of doing the check soon.

ligbksu/gksu-context.c:
  - Only save the password in the keyring if try_need_password()
    has been called.
  - Call try_su_run() before the gnome-keyring test; this solves
    lots of issues I was work-arounding before.
  - Fix password checking to correct number of chars to compare and
    close fdpty after the comparison so su will not be blocked
    trying to write to the terminal.
  - Avoid using the keyring if su will simply work without a
    password or if the password is not there at the time it needs
    to be saved.
  - Nullify the password when entering try_need_password() and if
    su will run without it (it may have been reset by
    try_gnome_keyring()).
  - Implement a new test to simply try to run su and see what
    happens to check if we need a password.
  - The above involved some changes to the _run method so it won't
    simply bail out if a password is not supplied and will also be
    more careful while discarding su output, which will simply not
    exist in some cases.

Add a Romanian translation.
(https://bugs.debian.org/309532)

Release 1.3.0 (2005-06-13 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/gksu-context.c:
  - Make the process which uses libgksu a session leader, thanks to
    Michael Vogt <mvogt@acm.org>.

configure.ac, libgksu/Makefile.am:
  - Provide a --enable-gnome-keyring option to configure, disabled
    by default, based on a patch by
    Szilard Novaki <novaki@agmen-software.com>.

libgksu/gksu-context.{c,h}, docs/libgksu1.2-sections.txt:
  - Add a new public API function gksu_context_try_need_pass() to
    check if we need to request a password (API version went up to
    0:1:0, keeping compatibility).
  - Implement a method of getting a password from and saving it to
    the gnome-keyring, based on a patch by Szilard Novaki.

Update translations.

Release 1.2.6 (2005-04-05 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix a problem dealing with different xauth cookie protocols which
makes it work with xdm and remote connections, it is hoped.
(https://bugs.debian.org/245092, https://bugs.debian.org/280914)

libgksu/gksu-context.c:
  - Apply a patch from Michael Vogt to have correct perms on the
    .Xauthority file that is created for sudo.
  - Some code cleanup and fix minor glitches.

libgksu/gksu-run-helper.c:
  - Changes to set XAUTHORITY in a more sane way and call the real
    command separately.

Release 1.2.5a (2004-10-17 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix sudo functionality which was b0rked by the last release.

Release 1.2.5 (2004-10-16 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fixes for correct reading the output of gksu-run-helper, reporting
wrong password typing.
(https://bugs.debian.org/272133)

No longer ignore context->keep_env when doing sudo stuff.
(https://bugs.debian.org/276720)

Better handling of remote usage, no longer segfaults when running
remotely.
(https://bugs.debian.org/273819)

configure.ac:
  - Add no_NB to ALL_LINGUAS.

Release 1.2.4 (2004-09-07 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Fix buffer overflows, thanks to Martin Pitt <martin.pitt@ubuntu.com>.
(https://bugs.debian.org/270485)

Release 1.2.3 (2004-08-15 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Add Catalan translation done by Jordi Mallach <jordi@debian.org>.
(https://bugs.debian.org/263288)

Release 1.2.2 (2004-06-24 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/gksu-context.h:
  - Add a missing G_END_DECLS to fix C++ pre-processing.
    (https://bugs.debian.org/255607)

Release 1.2.1 (2004-05-29 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/libgksu.ver, libgksu/Makefile.am:
  - Build the library with versioned symbols (thanks to
    Steve Langasek's [vorlon's] talk on "Escaping the dependency
    hell" at Debian Conference 4.

Release 1.2.0 (2004-05-18 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

libgksu/gksu-context.c:
  - Fix a string about Xauthority file already existing when being
    created for the target user.

docs/libgksu-sections.txt:
  - Remove the macros and the get_type function, add
    gksu_context_free.

libgksu/gksu-context.{c,h}:
  - Document GksuContext and gksu_context_free.

Release 1.1.5 (2004-05-05 Gustavo Noronha Silva <kov@debian.org>)
===================================================================

Change the license to LGPLv2+.

libgksu/gksu-main*, libgksu/gksu-context*:
  - Move -main stuff to -context, make the _run stuff be methods of
    the GksuContext object.
    Also wrote a get_type function to the enum to help bindings.

libgksu/gksu-context.{h,c}:
  - Remove title, message and icon related methods as they are now
    to be used with the gksuui-dialog widget available in libgksuui.
  - Fix problems with the naming of the GObject-related macros, so
    for example TYPE_GKSU_CONTEXT becomes GKSU_TYPE_CONTEXT -> I had
    misunderstood the docs and this seems to confuse the python
    binding generator.
  - New error for xauth stuff problems.
  - Look at the XAUTHORITY environment variable to get the source
    from which to copy the Xauthorization file.
    (https://bugs.debian.org/246516)

libgksu/gksu.h, gksu-context.h:
  - Change double quotes to <> in gksu.h, fixed double inclusion of
    glib.h and self-inclusion on gksu-context.h, thanks to
    Max Reinhold Jahnke for the latter fix.

libgksu/gksu-context.c:
  - Add some wait time before sending the password to su - the
    password was not being sent correctly without this.
  - Remove the password displaying debug stuff (it is not safe ;)).
  - Add an usleep to wait 200 usecs on each loop on the while that
    prints messages... it seems like linux 2.6's select() does not
    want to wait =)
  - More select->sleep to fix bugs in linux 2.6.

libgksu/test-gksu.c:
  - Change the command that is called from ls to xterm.

Release 1.1.1 (2004-03-06 Gustavo Noronha Silva <kov@debian.org>)
===================================================================
First release
