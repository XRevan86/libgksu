/*
 * gksu-properties -- properties dialog for gksu
 * Copyright (C) 2004-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <https://gnu.org/licenses>.
 */

#include <locale.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gio/gio.h>

#include "../config.h"
#include "../libgksu/defines.h"

static GtkBuilder *gui = NULL;
static GtkWidget *main_window;
static GtkWidget *grab_combo;
static GtkWidget *mode_combo;
static GSettings *gsettings; /* NULL */

gboolean disable_grab = FALSE;
gboolean force_grab = FALSE;
gboolean prompt = FALSE;
gboolean sudo_mode = FALSE;

void
update_from_gsettings ()
{
  disable_grab = g_settings_get_boolean (gsettings, "disable-grab");

  force_grab = g_settings_get_boolean (gsettings, "force-grab");

  prompt = g_settings_get_boolean (gsettings, "prompt");

  sudo_mode = g_settings_get_boolean (gsettings, "sudo-mode");
}

const char *
get_grab_string ()
{
  if (prompt)
    return "prompt";

  if (force_grab)
    return "force enable";

  if (disable_grab)
    return "disable";

  return "enable";
}

void
update_grab_combo ()
{
  GtkTreeModel *model;
  GtkTreeIter iter;

  const char *tmp = NULL;
  GValue buffer = G_VALUE_INIT;
  gboolean found = FALSE;

  model = gtk_combo_box_get_model (GTK_COMBO_BOX (grab_combo));
  gtk_tree_model_get_iter_first (model, &iter);
  tmp = get_grab_string ();
  do
    {
      gtk_tree_model_get_value (model, &iter, 1, &buffer);
      if (g_strcmp0 (g_value_get_string (&buffer), tmp) == 0)
	{
	  g_value_unset (&buffer);
	  found = TRUE;
	  break;
	}
      g_value_unset (&buffer);
    } while (gtk_tree_model_iter_next (model, &iter));

  if (found)
    gtk_combo_box_set_active_iter (GTK_COMBO_BOX (grab_combo), &iter);
}

void
update_mode_combo ()
{
  GtkTreeModel *model;
  GtkTreeIter iter;

  GValue buffer = G_VALUE_INIT;
  const char *tmp = NULL;
  gboolean found = FALSE;

  model = gtk_combo_box_get_model (GTK_COMBO_BOX (mode_combo));
  gtk_tree_model_get_iter_first (model, &iter);
  if (sudo_mode)
    tmp = "sudo";
  else
    tmp = "su";
  do
    {
      gtk_tree_model_get_value (model, &iter, 1, &buffer);
      if (g_strcmp0 (g_value_get_string (&buffer), tmp) == 0)
	{
	  g_value_unset (&buffer);
	  found = TRUE;
	  break;
	}
      g_value_unset (&buffer);
    } while (gtk_tree_model_iter_next (model, &iter));

  if (found)
    gtk_combo_box_set_active_iter (GTK_COMBO_BOX (mode_combo), &iter);
}

void
create_dialog ()
{
  GdkPixbuf *icon;
  main_window = GTK_WIDGET (gtk_builder_get_object (gui, "main_window"));
  icon = gdk_pixbuf_new_from_file (DATA_DIR "/icons/hicolor/48x48/apps/gksu.png", NULL);
  if (icon)
    gtk_window_set_icon (GTK_WINDOW(main_window), icon);
  else
    g_warning ("Error loading window icon %s",
	       DATA_DIR "/icons/hicolor/48x48/apps/gksu.png\n");

  grab_combo = GTK_WIDGET (gtk_builder_get_object (gui, "grab_combo"));
  update_grab_combo ();

  mode_combo = GTK_WIDGET (gtk_builder_get_object (gui, "mode_combo"));
  update_mode_combo ();

  gtk_widget_show_all (main_window);
}

void
gsettings_change_cb (GSettings *settings, guint cnxn_id,
		 char *key, gpointer data)
{
  update_from_gsettings ();
  update_mode_combo ();
  update_grab_combo ();
}

void
combo_change_cb (GtkWidget *widget, gpointer data)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  GtkComboBox *combo_box = GTK_COMBO_BOX (widget);
  char *combo_name = (char *) data;

  GValue buffer = G_VALUE_INIT;

  model = gtk_combo_box_get_model (combo_box);
  if (gtk_combo_box_get_active_iter (combo_box, &iter))
    {
      gtk_tree_model_get_value (model, &iter, 1, &buffer);
      if (g_strcmp0 (combo_name, "mode") == 0)
	{
	  if (g_strcmp0 (g_value_get_string (&buffer), "sudo") == 0)
	    g_settings_set_boolean (gsettings, "sudo-mode", TRUE);
	  else
	    g_settings_set_boolean (gsettings, "sudo-mode", FALSE);
	}
      else
	{
	  if (g_strcmp0 (g_value_get_string (&buffer), "enable") == 0)
	    {
	      g_settings_set_boolean (gsettings, "prompt", FALSE);
	      g_settings_set_boolean (gsettings, "disable-grab", FALSE);
	      g_settings_set_boolean (gsettings, "force-grab", FALSE);
	    }
	  else if (g_strcmp0 (g_value_get_string (&buffer), "disable") == 0)
	    {
	      g_settings_set_boolean (gsettings, "prompt", FALSE);
	      g_settings_set_boolean (gsettings, "disable-grab", TRUE);
	      g_settings_set_boolean (gsettings, "force-grab", FALSE);
	    }
	  else if (g_strcmp0 (g_value_get_string (&buffer), "prompt") == 0)
	    {
	      g_settings_set_boolean (gsettings, "prompt", TRUE);
	      g_settings_set_boolean (gsettings, "disable-grab", FALSE);
	      g_settings_set_boolean (gsettings, "force-grab", FALSE);
	    }
	  else if (g_strcmp0 (g_value_get_string (&buffer), "force enable") == 0)
	    {
	      g_settings_set_boolean (gsettings, "prompt", FALSE);
	      g_settings_set_boolean (gsettings, "disable-grab", FALSE);
	      g_settings_set_boolean (gsettings, "force-grab", TRUE);
	    }
	}
      g_value_unset (&buffer);
    }
}

void
setup_notifications ()
{
  g_signal_connect (gsettings, "changed::" GSETTINGS_SCHEMA,
		    G_CALLBACK(gsettings_change_cb), NULL);

  g_signal_connect (G_OBJECT(mode_combo), "changed",
		    G_CALLBACK(combo_change_cb), "mode");

  g_signal_connect (G_OBJECT(grab_combo), "changed",
		    G_CALLBACK(combo_change_cb), "grab");
}

int
main (int argc, char **argv)
{
  GError* error = NULL;

  bindtextdomain (PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (PACKAGE, "UTF-8");
  textdomain (PACKAGE);

  gtk_init (&argc, &argv);

  if (g_file_test ("gksu-properties.ui", G_FILE_TEST_EXISTS) == TRUE)
    {
      gui = gtk_builder_new ();
      if (!gtk_builder_add_from_file (gui, "gksu-properties.ui", &error))
        {
          g_warning ("Couldn't load builder file: %s", error->message);
          g_error_free (error);
        }
    }
  else if (g_file_test (DATA_DIR "/" PACKAGE "/gksu-properties.ui",
			G_FILE_TEST_EXISTS) == TRUE)
    {
      gui = gtk_builder_new ();
      if (!gtk_builder_add_from_file (gui, DATA_DIR "/" PACKAGE "/gksu-properties.ui", &error))
        {
          g_warning ("Couldn't load builder file: %s", error->message);
          g_error_free (error);
        }
    }

  if (!gui) {
    GtkWidget *dialog;

    dialog = gtk_message_dialog_new (NULL,
				     0,
				     GTK_MESSAGE_ERROR,
				     GTK_BUTTONS_CLOSE,
				     _("Failed to load gtkui file; please check your installation."));

    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);

    return 1;
  }

  gtk_builder_connect_signals (gui, NULL);

  gsettings = g_settings_new (GSETTINGS_SCHEMA);

  update_from_gsettings ();
  create_dialog ();
  setup_notifications ();

  if (main_window)
    gtk_main ();

  g_object_unref (gsettings);

  return 0;
}
